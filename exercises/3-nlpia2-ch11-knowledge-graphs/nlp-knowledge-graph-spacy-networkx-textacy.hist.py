nlp = spacy.load('en_core_web_sm')
import spacy
nlp = spacy.load('en_core_web_sm')
from collections import Counter
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
# import re   

import warnings
warnings.filterwarnings("ignore")

## for data

## for plotting
import matplotlib.pyplot as plt  #3.3.2
import seaborn as sns  #0.11.1

## for text
import wikipediaapi  #0.5.8

import spacy  #3.5.0
from spacy import displacy
import textacy  #0.12.0

## for graph
import networkx as nx  # 3.0 (also pygraphviz==1.10)
topic = "Cory Doctorow"
wiki = wikipediaapi.Wikipedia('en')
page = wiki.page(topic)
txt = page.text[:page.text.find("See also")]
txt[0:500] + " ..."
%run src/grounder/knowledge_graph_tutorial.py
dtf
create_subgraph(entity='he')
create_subgraph(dtf, entity='he')
g = _
plot_subgraph(G, entity='he')
columns = 'text pos_ dep_'.split()
df_sent = pd.DataFrame([[getattr(t, c) for c in columns] for t in sent], columns=columns)
df_sent
def extract_relation(doc, nlp):
    matcher = spacy.matcher.Matcher(nlp.vocab)
    pattern = [{'DEP':'ROOT'}, 
          {'DEP': 'prep', 'OP':"?"},
          {'DEP': 'agent', 'OP':"?"},
          {'POS': 'ADJ', 'OP':"?"}] 
    matcher.add(key="root_prep_agent_adj", patterns=[pattern]) 
    matches = matcher(doc)
    k = len(matches) - 1
    span = doc[matches[k][1]:matches[k][2]] 
    return span.text
def extract_relation(sents[3], nlp):
    matcher = spacy.matcher.Matcher(nlp.vocab)
    pattern = [{'DEP':'ROOT'}, 
          {'DEP': 'prep', 'OP':"?"},
          {'DEP': 'agent', 'OP':"?"},
          {'POS': 'ADJ', 'OP':"?"}] 
    matcher.add(key="root_prep_agent_adj", patterns=[pattern]) 
    matches = matcher(doc)
    k = len(matches) - 1
    span = doc[matches[k][1]:matches[k][2]] 
    return span.text
sents
docs.sents[3]
doc.sents[3]
sents list(doc.sents)
sents = list(doc.sents)
def extract_relation(sents[3], nlp):
    matcher = spacy.matcher.Matcher(nlp.vocab)
    pattern = [{'DEP':'ROOT'}, 
          {'DEP': 'prep', 'OP':"?"},
          {'DEP': 'agent', 'OP':"?"},
          {'POS': 'ADJ', 'OP':"?"}] 
    matcher.add(key="root_prep_agent_adj", patterns=[pattern]) 
    matches = matcher(doc)
    k = len(matches) - 1
    span = doc[matches[k][1]:matches[k][2]] 
    return span.text
extract_relation(sent=sents[3])
extract_relation(sents[3], nlp=nlp)
extract_entities(sents[3])
sents[3].ents
dtf
sents[81]
sents[81].ents
plot_subgraph(G)
    fig = plt.figure(figsize=(15, 10))

    pos = nx.spring_layout(G, k=1)
    # pos = nx.nx_agraph.graphviz_layout(G, prog="neato")

    node_color = ["red" if node==entity else "skyblue" for node in G.nodes]
    edge_color = ["red" if edge[0]==entity else "black" for edge in G.edges]

    nx.draw(G, pos=pos, with_labels=True, node_color=node_color, 
            edge_color=edge_color, cmap=plt.cm.Dark2, 
            node_size=2000, node_shape="o", connectionstyle='arc3,rad=0.1')
    nx.draw_networkx_edge_labels(G, pos=pos, label_pos=0.5, 
                            edge_labels=nx.get_edge_attributes(G,'relation'),
                            font_size=12, font_color='black', alpha=0.6)
    return fig
    fig = plt.figure(figsize=(15, 10))

    pos = nx.spring_layout(G, k=1)
    # pos = nx.nx_agraph.graphviz_layout(G, prog="neato")

    node_color = ["red" if node==entity else "skyblue" for node in G.nodes]
    edge_color = ["red" if edge[0]==entity else "black" for edge in G.edges]

    nx.draw(G, pos=pos, with_labels=True, node_color=node_color, 
            edge_color=edge_color, cmap=plt.cm.Dark2, 
            node_size=2000, node_shape="o", connectionstyle='arc3,rad=0.1')
    nx.draw_networkx_edge_labels(G, pos=pos, label_pos=0.5, 
                            edge_labels=nx.get_edge_attributes(G,'relation'),
                            font_size=12, font_color='black', alpha=0.6)
node_color
entity = 'he'
node_color
G.nodes
G.edges
G = create_subgraph(dtf)
G
G.nodes
G.edges
G = create_subgraph(dtf, entity=None)
G.edges
def create_subgraph(dtf, entity='Cory Efram Doctorow'):
    entity = entity.strip().replace(' ', '_').lower()
    if not entity or str(entity).lower().strip() == 'all':
        entity = None
    if entity:
        tmp = dtf[(dtf["entity"].str.strip().str.lower()==entity) | (dtf["object"].str.strip().str.lower()==entity)]
    else:
        tmp = dtf[(dtf["entity"].str.len() > 0) | (dtf["object"].str.len() > 0)]

    G = nx.from_pandas_edgelist(
        tmp, source="entity", target="object", 
        edge_attr="relation", 
        create_using=nx.DiGraph())
    return G

G = create_subgraph(dtf, entity=None)

def create_subgraph(dtf, entity='Cory Efram Doctorow'):
    entity = entity.strip().replace(' ', '_').lower() if entity else None
    if not entity or str(entity).lower().strip() == 'all':
        entity = None
    if entity:
        tmp = dtf[(dtf["entity"].str.strip().str.lower()==entity) | (dtf["object"].str.strip().str.lower()==entity)]
    else:
        tmp = dtf[(dtf["entity"].str.len() > 0) | (dtf["object"].str.len() > 0)]

    G = nx.from_pandas_edgelist(
        tmp, source="entity", target="object", 
        edge_attr="relation", 
        create_using=nx.DiGraph())
    return G

G = create_subgraph(dtf, entity=None)
G.edges
def plot_subgraph(G, show=True, entity=None):
    """ FIXME
    File ~/code/tangibleai/nlpia2/.venv/lib/python3.9/site-packages/pygraphviz/agraph.py:336, in AGraph.add_node(self, n, **attr)
    334 except KeyError:
    335     nh = gv.agnode(self.handle, n, _Action.create)
    --> 336 node = Node(self, nh=nh)
        337 node.attr.update(**attr)

    File ~/code/tangibleai/nlpia2/.venv/lib/python3.9/site-packages/pygraphviz/agraph.py:1857, in Node.__new__(self, graph, name, nh)
       1855 def __new__(self, graph, name=None, nh=None):
       1856     if nh is not None:
    -> 1857         n = super().__new__(self, gv.agnameof(nh), graph.encoding)
       1858     else:
       1859         n = super().__new__(self, name)

    TypeError: decoding to str: need a bytes-like object, NoneType found
"""
    fig = plt.figure(figsize=(15,10))

    # pos = nx.spring_layout(G, k=1)
    pos = nx.nx_agraph.graphviz_layout(G, prog="neato")

    node_color = ["red" if node==entity else "skyblue" for node in G.nodes]
    edge_color = ["red" if edge[0]==entity else "black" for edge in G.edges]

    nx.draw(G, pos=pos, with_labels=True, node_color=node_color, 
            edge_color=edge_color, cmap=plt.cm.Dark2, 
            node_size=2000, node_shape="o", connectionstyle='arc3,rad=0.1')
    nx.draw_networkx_edge_labels(G, pos=pos, label_pos=0.5, 
                            edge_labels=nx.get_edge_attributes(G,'relation'),
                            font_size=12, font_color='black', alpha=0.6)
    if show:
        plt.show()

    return fig
plot_subgraph(G)
diff = difflib.Differ()
import difflib
diff = difflib.Differ()
l = list(diff.compare("hello", "heXXo"))
l
sents[3]
lst_tokens[3]
toks = [t.text for t in sents[3]]
toks
from nlpia2.gec import *
who
corrector = Corrector()
corrector.correct(sents[3].text)
diff.compare(sents[3].text, corrector.correct(sents[3].text))
list(diff.compare(sents[3].text, corrector.correct(sents[3].text)))
corrected = corrector.correct(sents[3].text))
corrected = corrector.correct(sents[3].text)
diff([t.text for t in sents[3]], [t.text for t in nlp(corrected)])
diff?
diff.compare([t.text for t in sents[3]], [t.text for t in nlp(corrected)])
list(diff.compare([t.text for t in sents[3]], [t.text for t in nlp(corrected)]))
hist -f nlp-video3.hist.py
