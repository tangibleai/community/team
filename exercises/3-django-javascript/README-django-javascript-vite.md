# Organizing your Front-End Codebase in a Django Project

There are two main approaches for building a webapp: Back-End and Front-End. Let's consider them both:

![architectures somparison](./server-architectures-compare.png)

Generally, server-first architecture is bad because javascript code snippets dropped into the html template will make a code mess really quickly.
The client-first architecture disadvantage comes when you want to use some common functionality, for example form validation.
You could validate a form with a few lines of code in django, but it takes a lot more effort to do the same thing in javascript.
And if you want to creat a fast autocomplete widget, you will need to coordinate both the frontend and backend to manage a _cache_ of the most likely autocomplete words.
This is when a hybrid architecture such as AJAX is critical.
In a hybrid architecture each web page of your app can use a client or server-first design, and even do both in the same page (AJAX).

Remember, in a hybrid application we'll have all of the the following types of pages:

#### Server-first

Server-first Django pages with server-side rendering and little or no JavaScript.
Pure Django pages are the simplest and most straightforward to set up since they have basically zero front-end code.
All of your page can be rendered by Django within a `templates/*.html`.
The Django template syntax is similar to format-string (f-strings) or `jinja2` templates, so you may already know how to insert data from your database into your pages.

#### Client-first

Client-first JavaScript pages use very little Django server-side rendering are great for pages where you want to be respond quickly to each user keystroke or mouse click.
These are great for GUI applications where you have a complex UI that relies on a big JavaScript framework such as Vue, React, or Svelte.

## Modern JavaScript Tooling

The modern JavaScript tool chain

![javascript toolchain](./javascript-toolchain.jpg)

It's just three things!
Let's get into those in a little more depth.

### Package manager

The first part of the toolchain is the package manager. This is the thing that allows you to import and use packages that other people have built—basically pip but for JavaScript. There are two popular packages managers out there, Yarn, and npm. JavaScript people can have strong opinions about which one is best, but honestly they're both fine. Npm is more popular. Yarn is newer and does a few things NPM can't do.

### Bundler

The bundler's job is to take your code—the libraries you're using from the package manager plus any code that you've written—and, well bundle it up. Bundlers take code spread across a bunch of places and mash them together so you only have to drop small number of files on your site—making it more performant. The most popular one is webpack.

### The compiler (transpiler)

The compiler's job is take those and make them backwards compatible, so they work with the pesky devices—mostly browsers—that don't understand them yet. In this guide we will be using babel

![javascript toolchain](./javascript-toolchain-general-image.png)

# Integrating a Modern JavaScript Pipeline into a Django Application

### How JavaScript and Django will integrate

Basically, we're going to create the JavaScript pipeline we covered in previous section as a standalone, separate environment inside our Django project.

Then to integrate with Django, the outputs (remember those bundle files?) will be static files that we'll drop into our templates wherever we need them. This will allow us to create anything from single-page apps, to reusable JavaScript libraries while maintaining the best of Django and JavaScript tooling.

![javascript toolchain](./django-javascript-composition.jpg)

### Laying out your project

At a high level, it's suggested making your JavaScript project a subfolder in the root of your larger Django project git repo where your `manage.py` file lives.
This guide uses a folder called `assets/` for the front-end source files, but `vite.js` and other build systems often create a separate ``frontend/`` directory.


```console
├── manage.py
├── mysite     <-- Django project (manage.py startproject mysite)
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── myapp      <-- Django app (manage.py startapp myapp)
│   ├── __init__.py
│   ├── models.py
│   ├── urls.py
│   └── views.py
├── assets     <-- frontend source code before compiling with webpack/rollup/vite.js
│   ├── javascript
│   └── styles
├── static     <-- frontend bundles from webpack/rollup/vite.js & Django collectstatic
│   ├── css
│   ├── images
│   └── js  
└── templates  <----- Django (backend) template files
    └── myapp
```

All of your frontend code goes into `assets` and `static`.

Then follow these steps:
https://www.saaspegasus.com/guides/modern-javascript-for-django-developers/integrating-javascript-pipeline/#setting-up-webpack-inside-your-django-project

### How to build a React application in a Django project

# Single-page-applications in a hybrid architecture

Some pages—for example, a login page—might be traditional Django templates and forms, while other pages might be almost 100% JavaScript. In this section we cover that second category of pages. Pages we dubbed client-first that are essentially single-page-applications embedded inside a Django project.

# Authentication

Because we're serving our pages directly from Django we can just use all of Django's built-in tooling to handle authentication and not rely on complex third-party authentication workflows.

That means we can just use the @login_required decorator or LoginRequiredMixin on the hybrid Django view serving our template and Django handles the rest. No wrestling with CORS or 3rd-party authentication frameworks is necessary.

# Passing data from the back end to the front end

In a hybrid architecture there are two mechanisms to send data to the front end:

    - Pass the data directly to the template, using Django's built-in templating system.
    - Provide the data via asynchronous APIs, using JavaScript and Django REST framework (DRF).



### References 

In order of relevance for ConvoHub. TODO in this order:

0. [Three ways to deploy Django + React app](https://mattsegal.dev/django-spa-infrastructure.html)
1. [django_vite + Express example/tutorial](https://ctrlzblog.com/the-django-developers-guide-to-vite/)
2. [Tabulator.js for Svelte](https://tabulator.info/docs/5.5/svelte#svelte) (works with React, Angular, Vue too)
  - [Tabulator.js for Svelte](https://tabulator.info/docs/5.5/svelte#svelte) (works with React, Angular, Vue too)

3. [Svelte frontend + Express backend for chatbot web interface](https://dev.to/debussyman/svelte-without-kit-253l)
4. [vite.js features (dependency hoisting? prebundling)](https://v2.vitejs.dev/guide/features.html)
- [2022 svelte-django monolith architecture tutorial (vite.js)]https://github.com/ariroffe/django-svelte-integration-guide
- [Best practices for full stack development](https://mattsegal.dev/)
- https://ctrlzblog.com/the-django-developers-guide-to-vite/

#### Unrelated ConvoHub references and alternatives
- [Customize Django Admin](https://realpython.com/customize-django-admin-python/)
- [crisp.chat backend textit](https://www.textit.com/) alternatives:
  - [FOSS chaskiq (Ruby React Postgres Redis)](https://github.com/chaskiq/chaskiq)
  - Drift
  - Intercom




- [flowbite-svelte references](https://flowbite-svelte.com/docs/components/table#References)
  - [Flowbite Tables](https://flowbite.com/docs/components/tables/)
  - [Table component on GitHub](https://github.com/themesberg/flowbite-svelte/blob/main/src/lib/table/Table.svelte)
  - [TableBody component on GitHub](https://github.com/themesberg/flowbite-svelte/blob/main/src/lib/table/TableBody.svelte)
  - [TableBodyCell component on GitHub](https://github.com/themesberg/flowbite-svelte/blob/main/src/lib/table/TableBodyCell.svelte)
  - [TableBodyRow component on GitHub](https://github.com/themesberg/flowbite-svelte/blob/main/src/lib/table/TableBodyRow.svelte)
  - [TableHead component on GitHub](https://github.com/themesberg/flowbite-svelte/blob/main/src/lib/table/TableHead.svelte)
  - [TableHeadCell component on GitHub](https://github.com/themesberg/flowbite-svelte/blob/main/src/lib/table/TableHeadCell.svelte)
  - [TableSearch component on GitHub](https://github.com/themesberg/flowbite-svelte/blob/main/src/lib/table/TableSearch.svelte)


### Notes (consolidate)

These other notes and webclips should be consolidated into [THIS exercise](team/exercises/3-django-javascript/README-django-javascript-vite.md
)
- [notes/obsidian/2023-journal/2023-11-28 flowbite vite svelte.md](notes/obsidian/2023-journal/2023-11-28 flowbite vite svelte.md)
- [download_markdown_clips/2023-12-01-Svelte Tables - Flowbite.md](download_markdown_clips/2023-12-01-Svelte Tables - Flowbite.md)

