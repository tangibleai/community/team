# Best practices for starting a new Django project

Here are all the steps you need to start your new Django project. 
If you do it this way it will make it easier for your to come back to it after working on something else for a while.

## Key Project Files and Folders
|Name | Type | Location | Description | 
|---|---|---|---|
| `.gitignore` | File | `~/code/project_name/` | IMPORTANT (DO THIS FIRST): Copy a `.gitignore` file from another mature Tangible AI repository before doing any `git add` commands to avoid accidentally adding `.env` or other files that contain secrets. |
| `.env-dev` | File | `~/code/project_name/` | Environment variable definitions (bash syntax) for database and local (not on the Internet) service tokens and security credentials needed when running on your development environment (laptop within `localhost` domain). Preferrably none of these should point to external (Internet) services and should be usable by any developer on their local machine only. | 
| `.env-prod` | File | `~/code/project_name/` | Copied from BitWarden. Environment variable definitions (bash syntax) for database and external service URLs, tokens, and security credentials needed when running on a production VPS (e.g. Digital Ocean, Render.com) | 
| `requirements.txt` | File | `~/code/project_name/` | An up-to-date list of all project Python dependencies (Python package names & versions). It's usually best to "pin" this packages to a fixed version number. You can copy this file from another similar project at Tangible AI to help reduce the `pip install` time (because that will cause it to reuse your cached Python package `.whl` files). |
| CHANGELOG.md | File | /docs | Notes and version numbers for features added or removed with each release (deployment to a Public user-facing server) or publication on PyPi as a pip-installable package. |
| build.sh | File | `~/code/project_name/` | Bash script to install dependencies, migrate databases, load databased fixtures (CSV, YAML, JSON), create users or superusers on a VPS (e.g. render.com). |
| start.sh | File | `~/code/project_name/` | Bash script that launches gunicorn (wsgi.py) or daphne (asgi.py) services that provide port 8000 or 80 endpoints to be proxied by nginx or other web servers. Usually run on the VPS (render.com), but can sometimes also be run in a dev environment. |
| render.yaml | File | `~/code/project_name/` |Contains the settings necessary to trigger render to set up a new application and/or service |
| config.py | File | `~/code/project_name/` | A central place to put all configuration values.  It should **not** contain sensitive information, such as keys or passwords.  .env is referenced to get sensitive values |
| /docs | Folder |  `~/code/project_name/` | Sprint plans, documentation, and ideation.  Files usually are Markdown (.md) files.|
| /scripts | Folder | `~/code/project_name/` | Small scripts that do very specific tasks not associated with a specific Django application |


## Python
Use Python >=3.10 unless dependencies require something else.

## Project setup
1. Make sure that all the key files and folders above are present
2. Make sure the following packages are incorporated into the project:

```toml
django = ">=4.1.0"
django-debug-toolbar = ">=3.7.0"
django-extensions==3.2.1
gunicorn
pip>=22.2.2
python-dotenv  
whitenoise
```   

3. Load in the .env values in config.py.

```python
from dotenv import load_dotenv, dotenv_values

load_dotenv()  # create os.environ['VARIABLE_NAME']=VALUE

CONFIG = dotenv_values()  # create `dict(VARIABLE_NAME=VALUE, ...)`
```


## Git project setup
1. Set up a Git repository
2. Clone git repository to your local environment



## Setting up Render.com

1. Use the free plan unless it's necessary to use Shell or Logs.  Also, if the service is slow, you can upgrade to the Starter plan.

2. render.yaml has the project settings.  Render.com uses builds a unique domain name for the project.  If the project name is "stigsite-tec", Render.com will use "stigsite-tec" if no one else is using it.  If that is not available, it will append -{four characters} to make a unique URL.  Set the domain as you can see in the sample yaml file below.

3. Render.com offers two ways of setting environment variables - individual environment variable fields and an .env file.  Most Environment variables should be set in the .env file.  PYTHON_VERSION and WEB_CONCURRENCY do not need to be included in the .env because Render.com will automatically set these based on the sample yaml file below.

3. Render.com will use the render.yaml file (sample below) to set up new instances of your web application.  If it is associated with a Blueprint, it will create new instances of your web application on each deployment (ie., update).  After the initial creation, turn off Auto Sync for any associated blueprint to avoid making unnecessary applications/services (Blueprint > Settings > Auto Sync = No).

```
services:
  - type: web
    name: stigsite-tec
    branch: main
    plan: free
    env: python
    buildCommand: "source ./build.sh"
    startCommand: "source ./start.sh"
    envVars:
      - key: SECRET_KEY
        generateValue: true
      - key: WEB_CONCURRENCY
        value: 4
      - key: PYTHON_VERSION
        value: 3.9.7
    autoDeploy: false
    domains:
      - stigsite-tec.onrender.com

  - type: web
    name: stigsite-dogfood
    branch: staging
    plan: free
    env: python
    buildCommand: "source ./build.sh"
    startCommand: "source ./start.sh"
    envVars:
      - key: SECRET_KEY
        generateValue: true
      - key: WEB_CONCURRENCY
        value: 4
      - key: PYTHON_VERSION
        value: 3.9.7
    autoDeploy: false
    domains:
      - stigsite-dogfood.onrender.com
```



## Feature Work
1. Create a new branch from the source branch you want.

```
git checkout -b feature-{descriptive-name} main 
```

2. Work on the branch.  Push regularly to your branch.

```
git commit -m "{descriptive message}"
git push
```

3. Merge from one branch when you are ready to test or deploy your work on another branch (ie., feature branch > staging or staging > main).  The process for merging is as follows:
   a. Push your current work to the branch you are working on, such as the feature-branch.

```
      git push {feature-branch}
```
   b. Checkout the branch you want to merge into, such as staging.  Pull all current changes from the branch.

```
      git checkout {target-branch}
      git pull
```

   c. Checkout the branch you want to merge again (ie., the feature branch).  Merge the target-branch into the feature-branch.

```
      git checkout {feature-branch}
      git merge {target-branch}
      git push
```

   d. Checkout the branch you want to merge into again.  Merge the featurebranch into the target branch.

```
      git checkout {target-branch}
      git merge {feature-branch}
      git push
```

4. When you finish work on a feature branch, you can delete it.  A feature branch is generally not necessary when you have merged it to staging and do not anticipate working on the feature branch anymore.  
    
    **NOTE:** You should not delete the staging branch after merging it into main.

```
  git branch -d {feature-branch}
  git push -d origin {feature-branch}
```

5. When you are ready to merge staging into main, you should update CHANGELOG.md and push that to staging.  The CHANGELOG entry should contain: 1) a link to the tag as the entry title (even if the tag isn't made yet), 2) a descriptive phrase for the release (ie., "initial release"), 3) a list of significant user/client-focused changes.

6. After you merge staging into main, add a tag to the main branch.  The version number should only have numbers and decimals (ie., 1.5.28).

    **NOTE:** The base number #.#.XX should match the sprint plan number.  For example, the version for Sprint 13 would look like #.#.13.

```
  git checkout main
  git tag -a {version number} -m {description}
  git push --tags
```




## Examples and Resources

### Render.com
  * [render.yaml](https://render.com/docs/blueprint-spec)

### Tags
  * Tags are at the top of the main repository page
    - [Stigsite Tags](https://gitlab.com/tangibleai/stigsite/-/tags)
    - [Nudger Tags](https://gitlab.com/tangibleai/nudger/-/tags)

  * Tags are linked in the Changelog
    - [Stigsite Changelog](https://gitlab.com/tangibleai/stigsite/-/blob/main/docs/CHANGELOG.md)

### Resources
  * [Git SCM Documentation](https://git-scm.com/book/en/v2/Git-Basics-Tagging)
  * [Atlassian Documentation](https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag)
