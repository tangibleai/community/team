### Node

Node is a JavaScript and typescript compiler that you can use to run JavaScript directly on your desktop OS rather than in your browser.
It's common for most web designs to be developed using a node web server to simulate the production environment.
And in some cases having a separate node webserver handling all the static assets is more efficient than serving them within a conventional Python dynamic page server (gunicorn, Daphne for server-side rendering of Django templates).
A third option is to use an `nginx` server to handle static assets (JavaScript, images, CSS) separate from your dynamic html templates and python views.

This example app uses node for the development server, so make sure you have `node` and `npm` installed on your Linux desktop where you do your coding.
You may also want to install a node version manager such as `nvm` if you plan to work on more than one frontend (javascript) application.

#### Vite

The `vite.js` node package can be used to build (compile) typescript and JavaScript frontend (static) apps, especially those using frameworks such as Svelte, React and Vue.
When the vite dev server is running it will automatically detect when you edit your typescript or html.
It will automatically rebuild and bundle it in the dist/ directory and deploy it (copy minimized `rollup` bundle files) to the server static assets dir.

### Example app

If you don't already have a Svelte frontend app then create one using vite.js:

```bash
npm create vite@latest myapp -- --template svelte
cd myapp
pnpm install
```

### Flowbite

Flowbite is a collection of utility classes (reusable html components) based on the Tailwind CSS framework
Flowtbite utility classes (components) require Tailwind CSS:

```bash
npx svelte-add@latest tailwindcss
pnpm install
```

Run a local development server:

```bash
pnpm dev
```

Install Flowbite-svelte dependencies and libraries:

```bash
pnpm i -D flowbite-svelte flowbite
```


Update the tailwind.config.cjs file from your root project folder to let the Tailwind CSS compiler know where to look for the utility classes and also set up the Flowbite plugin.

In the provided code below, you can customize the primary color by modifying the appropriate color values. To change the primary color, simply uncomment the desired color object and modify the corresponding color values as needed.

```js
const config = {
  content: ['./src/**/*.{html,js,svelte,ts}', './node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'],

  plugins: [require('flowbite/plugin')],

  darkMode: 'class',

  theme: {
    extend: {
      colors: {
        // flowbite-svelte
        primary: {
          50: '#FFF5F2',
          100: '#FFF1EE',
          200: '#FFE4DE',
          300: '#FFD5CC',
          400: '#FFBCAD',
          500: '#FE795D',
          600: '#EF562F',
          700: '#EB4F27',
          800: '#CC4522',
          900: '#A5371B'
        }
      }
    }
  }
};

module.exports = config;
```

Now you should be able to work with the Flowbite Svelte library and imp