# Life Coach Chatbot

> ## Excerpt
> The Life Coach Chatbot

---
The Life Coach Chatbot

What is the secret to happiness?  
  
Let’s face it, there really isn’t one. Often times, though, we can find a resolution through self-reflection and it can help to talk it through with someone.

Burns had an interesting approach, which involves looking at an event that is haunting you and eke it out in a notebook. From here, you self-reflect on what negative feelings you’ve associated with the event, rate them, identify the distortions, and reflect on whether or not the negativity is a truth.

#### Burns How to Be Happy worksheet
[![Burns How to Be Happy worksheet](./burns-worksheet.png)](./burns-worksheet.png)

[![Burns How to Be Happy worksheet footer (rating)](./burns-worksheet-footer.png)](./burns-worksheet-footer.png)

#### Conversation design block diagram
[![Kalika's conversation design block diagram](./life-skill-dialog-tree.drawio.png)](./life-skill-dialog-tree.drawio.png)

We took this approach and placed it into a chatbot to help channel people into this sort of reflection. I started out by creating a life.py skill to drive the dialog that is stored in a YAML file.

After pointing this new file to the YAML file, I built a simple flow that asks you for negative emotions, to rate the emotion, to identify a negative distortion, and then tells you to re-rate your experience.

From here; we built a keyword match list to identify the keywords that may be associated with each state; set as a separate YAML.

A linear regression model was created, tested, and set as a function to select the keywords. It is at this point we enabled the NLP functions that are available in qary to detect the user’s intent; resulting in a form of fuzzy-keyword match.

An update to the python file that was created to drive the YAML dialog was made to make use of this fuzzy keyword approach.

\[DEMO\]

Enhancements

I think it’d be nice if we could get a journal going per user – so that they may review their progress(es). Burns uses a triple column technique for reflection wherein the author is to write out their automatic response and their belief in each one; identify the distortion; and then provide the rational response.

I missed the rational response step, favoring a rate-distort-re-rate approach then jumped straight to the outcome. I wonder whether or not this triple column technique – wherein the user has to step back and reflect on the rational response may enhance their experience and further their quest for happiness. It’s definitely where I would go next with this chatbot.

Experience/Lessons Learned

I learned a lot about how to work in an environment where many people are working on a project at the same time. I faced and resolved more than a few challenges with GitHub. I also learned a great deal about self-reflection and the 6 key steps to happiness. There were probably some programming things I learned in there, too – like how to maximize my work in sublime, command line work with iPython, common debugging techniques for agile programming, and there’s a great deal of confidence building.
