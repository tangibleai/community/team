# Where to host your generative model?

Here are some common LLM hosting services or platforms:

1. [Hugging Face](huggingface.co) - unreliable/slow servers?
1. [Together.AI](Together.AI) - cheap nonfree pricing options
1. https://openrouter.ai - free options, but requires CC and minimum balance
1. https://nlpcloud.com - no image description API, just text gen
1. MosaicML.com - just an LLM
1. [Octo.ai](Octo.ai)
1. [Replicate.com](replicate.com)

## 1. Huggingface

Hugging Face has lead the way in Ethical AI and democratizing AI tools.
In most cases it is free for you to host your model, API, or web app.
And there are many open source pretrained models on Huggingface that you can use for free.

## 2. TogetherAI

#### Summary
Founded by Stanford reserachers. Pricing privileges open source models.
Early access is cheap.

#### Use cases
Vishvesh Bhat uses it for Clevrly.io and a stealth virtual assistant startup.

#### Pricing 

$0.90e-6 / token for CodeLlamma-70B
$0.01e-6 / token for Llamma2 chat

## 2. OpenRouter.ai

#### Summary
OpenRouter has an 'Auto' model that will choose a model that best matches the prompt you need completed.
You can use the OpenRouter API to retrieve the latest list of available models and their prices: https://openrouter.ai/api/v1/models .
Llama2 models seem to be 1000 times cheaper than on NLPCloud 

#### Use cases
Thomas Meschede uses openrouter for his [xyntopia.com] [TaskyOn](tasyon.xyntopia.com) -- [source](https://github.com/Xyntopia/taskyon).


#### [_`pricing_openrouter_2023-12-13.csv`_](./pricing_openrouter_2023-12-13.csv)
| name                                  |   context_length | architecture   |   centpertok_prompt |   centpertok_complet |   cent_200tokprompt_200tokresp |
|:--------------------------------------|-----------------:|:---------------|--------------------:|---------------------:|-------------------------------:|
| OpenAI: GPT-4 32k                     |            32767 | GPT            |           0.006     |            0.012     |                        3.6     |
| OpenAI: GPT-4 32k (older v0314)       |            32767 | GPT            |           0.006     |            0.012     |                        3.6     |
| OpenAI: GPT-4 (older v0314)           |             8191 | GPT            |           0.003     |            0.006     |                        1.8     |
| OpenAI: GPT-4                         |             8191 | GPT            |           0.003     |            0.006     |                        1.8     |
| OpenAI: GPT-4 Vision (preview)        |           128000 | GPT            |           0.001     |            0.003     |                        0.8     |
| OpenAI: Davinci 2                     |             4095 | GPT            |           0.002     |            0.002     |                        0.8     |
| OpenAI: GPT-4 Turbo (preview)         |           128000 | GPT            |           0.001     |            0.003     |                        0.8     |
| Anthropic: Claude v2.1                |           200000 | Claude         |           0.0008    |            0.0024    |                        0.64    |
| Anthropic: Claude v2.0                |           100000 | Claude         |           0.0008    |            0.0024    |                        0.64    |
| Anthropic: Claude v1                  |             9000 | Claude         |           0.0008    |            0.0024    |                        0.64    |
| Anthropic: Claude 100k v1             |           100000 | Claude         |           0.0008    |            0.0024    |                        0.64    |
| Anthropic: Claude (older v1)          |             9000 | Claude         |           0.0008    |            0.0024    |                        0.64    |
| Goliath 120B                          |             6144 | Llama2         |           0.0009375 |            0.0009375 |                        0.375   |
| Neural Chat 7B v3.1                   |             4096 | Mistral        |           0.0005    |            0.0005    |                        0.2     |
| Nous: Hermes 2 Vision 7B (alpha)      |             4096 | Mistral        |           0.0005    |            0.0005    |                        0.2     |
| Llava 13B                             |             2048 | Llama2         |           0.0005    |            0.0005    |                        0.2     |
| Xwin 70B                              |             8192 | Llama2         |           0.000375  |            0.000375  |                        0.15    |
| Synthia 70B                           |             8192 | Llama2         |           0.000375  |            0.000375  |                        0.15    |
| Anthropic: Claude Instant v1          |           100000 | Claude         |           0.000163  |            0.000551  |                        0.1428  |
| Anthropic: Claude Instant 100k v1     |           100000 | Claude         |           0.000163  |            0.000551  |                        0.1428  |
| Anthropic: Claude Instant (older v1)  |             9000 | Claude         |           0.000163  |            0.000551  |                        0.1428  |
| OpenAI: GPT-3.5 Turbo 16k             |            16385 | GPT            |           0.0003    |            0.0004    |                        0.14    |
| Mancer: Weaver (alpha)                |             8000 | Llama2         |           0.0003375 |            0.0003375 |                        0.135   |
| Noromaid 20B                          |             8192 | Llama2         |           0.000225  |            0.000225  |                        0.09    |
| Perplexity: PPLX 70B Chat             |             4096 | Llama2         |           7e-05     |            0.00028   |                        0.07    |
| OpenAI: GPT-3.5 Turbo Instruct        |             4095 | GPT            |           0.00015   |            0.0002    |                        0.07    |
| OpenAI: GPT-3.5 Turbo 16k (preview)   |            16385 | GPT            |           0.0001    |            0.0002    |                        0.06    |
| OpenAI: GPT-3.5 Turbo                 |             4095 | GPT            |           0.0001    |            0.0002    |                        0.06    |
| OpenAI: GPT-3.5 Turbo (older v0301)   |             4095 | GPT            |           0.0001    |            0.0002    |                        0.06    |
| Perplexity: PPLX 70B Online           |             4096 | Llama2         |           0         |            0.00028   |                        0.056   |
| Pygmalion: Mythalion 13B              |             8192 | Llama2         |           0.0001125 |            0.0001125 |                        0.045   |
| MythoMax 13B 8k                       |             8192 | Llama2         |           0.0001125 |            0.0001125 |                        0.045   |
| ReMM SLERP 13B                        |             6144 | Llama2         |           0.0001125 |            0.0001125 |                        0.045   |
| Psyfighter v2 13B                     |             4096 | Llama2         |           0.0001    |            0.0001    |                        0.04    |
| Psyfighter 13B                        |             4096 | Llama2         |           0.0001    |            0.0001    |                        0.04    |
| Nous: Hermes 70B                      |             4096 | Llama2         |           9e-05     |            9e-05     |                        0.036   |
| Meta: Llama v2 70B Chat               |             4096 | Llama2         |           7e-05     |            9.5e-05   |                        0.033   |
| Airoboros 70B                         |             4096 | Llama2         |           7e-05     |            9.5e-05   |                        0.033   |
| lzlv 70B                              |             4096 | Llama2         |           7e-05     |            9.5e-05   |                        0.033   |
| Yi 34B Chat                           |             4096 | Llama2         |           8e-05     |            8e-05     |                        0.032   |
| Yi 34B (base)                         |             4096 | Llama2         |           8e-05     |            8e-05     |                        0.032   |
| MythoMax 13B                          |             4096 | Llama2         |           6e-05     |            6e-05     |                        0.024   |
| Mistral: Mixtral 8x7B (base)          |            32768 | Mistral        |           6e-05     |            6e-05     |                        0.024   |
| Google: PaLM 2 Code Chat 32k          |            32768 | PaLM           |           5e-05     |            5e-05     |                        0.02    |
| Google: PaLM 2 Code Chat              |             7168 | PaLM           |           5e-05     |            5e-05     |                        0.02    |
| Google: PaLM 2 Chat 32k               |            32768 | PaLM           |           5e-05     |            5e-05     |                        0.02    |
| Google: PaLM 2 Chat                   |             9216 | PaLM           |           5e-05     |            5e-05     |                        0.02    |
| Meta: CodeLlama 34B Instruct          |             8192 | Llama2         |           4e-05     |            4e-05     |                        0.016   |
| Phind: CodeLlama 34B v2               |             4096 | Llama2         |           4e-05     |            4e-05     |                        0.016   |
| Toppy M 7B                            |            32768 | Mistral        |           3.75e-05  |            3.75e-05  |                        0.015   |
| Nous: Capybara 34B                    |            32000 | Llama2         |           2.5e-05   |            2.5e-05   |                        0.01    |
| Meta: Llama v2 13B Chat               |             4096 | Llama2         |           2.345e-05 |            2.345e-05 |                        0.00938 |
| StripedHyena Hessian 7B (base)        |            32768 | Llama2         |           2e-05     |            2e-05     |                        0.008   |
| Mistral OpenOrca 7B                   |             8192 | Mistral        |           2e-05     |            2e-05     |                        0.008   |
| OpenHermes 2.5 Mistral 7B             |             4096 | Mistral        |           2e-05     |            2e-05     |                        0.008   |
| OpenHermes 2 Mistral 7B               |             4096 | Mistral        |           2e-05     |            2e-05     |                        0.008   |
| StripedHyena Nous 7B                  |            32768 | Llama2         |           2e-05     |            2e-05     |                        0.008   |
| Perplexity: PPLX 7B Chat              |             8192 | Llama2         |           7e-06     |            2.8e-05   |                        0.007   |
| Nous: Hermes 13B                      |             4096 | Llama2         |           1.5e-05   |            1.5e-05   |                        0.006   |
| Perplexity: PPLX 7B Online            |             4096 | Llama2         |           0         |            2.8e-05   |                        0.0056  |
| Yi 6B (base)                          |             4096 | Llama2         |           1.4e-05   |            1.4e-05   |                        0.0056  |
| Mistral 7B Instruct                   |             8192 | Mistral        |           0         |            0         |                        0       |
| Hugging Face: Zephyr 7B               |             4096 | Mistral        |           0         |            0         |                        0       |
| RWKV v5 World 3B (beta)               |            10000 | GPT            |           0         |            0         |                        0       |
| RWKV v5 3B AI Town (beta)             |            10000 | GPT            |           0         |            0         |                        0       |
| Cinematika 7B (alpha)                 |            32768 | Mistral        |           0         |            0         |                        0       |
| Mistral: Mixtral 8x7B Instruct (beta) |            32768 | Mistral        |           0         |            0         |                        0       |
| MythoMist 7B                          |            32768 | Mistral        |           0         |            0         |                        0       |
| OpenChat 3.5                          |             8192 | Mistral        |           0         |            0         |                        0       |
| Nous: Capybara 7B                     |             4096 | Mistral        |           0         |            0         |                        0       |
| Auto (best for prompt)                |           128000 | Router         |        -100         |         -100         |                   -40000       |


## 3. NLPCloud.com

You must verify both an e-mail and a mobile phone number in order to create an account and access the [free playground API](https://nlpcloud.com/home/playground/).

In 2023, the available free (playground) LLM chatbot models were ChatDolphin, Fine-Tuned LLAMA 70B, and Dolphin.

#### Example uses
- https://nlpcloud.com/nlp-text-classification-api.html
- https://nlpcloud.com/nlp-chatbot-conversational-ai-gpt-j-api.html
https://nlpcloud.com/nlp-automatic-speech-recognition-speech-to-text-api.html
- https://nlpcloud.com/nlp-source-code-generation-gpt-j-api.html
- https://nlpcloud.com/nlp-chatbot-conversational-ai-gpt-j-api.html
- https://nlpcloud.com/nlp-dialogue-summarization-api.html
- https://nlpcloud.com/nlp-embeddings-api.html
- https://nlpcloud.com/nlp-grammar-spelling-correction-gpt-j-api.html
- https://nlpcloud.com/nlp-text-summarization-api.html
- https://nlpcloud.com/nlp-image-generation-text-to-image-api-with-stable-diffusion-dalle-2-alternative.html
- https://nlpcloud.com/nlp-image-generation-text-to-image-api-with-stable-diffusion-dalle-2-alternative.html
- https://nlpcloud.com/nlp-intent-classification-detection-gpt-j-api.html
- https://nlpcloud.com/nlp-keyword-keyphrase-extraction-gpt-j-api.html
- https://nlpcloud.com/nlp-language-detection-api.html
- https://nlpcloud.com/nlp-tokenization-api.html
- https://nlpcloud.com/nlp-named-entity-recognition-ner-api.html
- https://nlpcloud.com/nlp-noun-chunks-noun-phrase-extraction-api.html
- https://nlpcloud.com/nlp-paraphrasing-gpt-j-api.html
- https://nlpcloud.com/nlp-part-of-speech-pos-tagging-api.html
- https://nlpcloud.com/nlp-question-answering-api.html
- https://nlpcloud.com/nlp-semantic-search-api.html
- https://nlpcloud.com/nlp-semantic-similarity-api.html
- https://nlpcloud.com/nlp-semantic-similarity-api.html
- https://nlpcloud.com/nlp-sentiment-analysis-api.html
- 


#### Pricing
- CPU: $0.003 / request
- GPU: $0.005 / request

- Dolphin/ChatDolphin: $0.00002 / token
- LLaMA 2 70B: $0.00007 / token
- Stable Diffusion: $0.05 / image
- Whisper: $0.0001 / s (duration of audio or video)
- Speech T5: $0.00003 / token

## 4. MosaicML.com

Mosaic ML is located in SF and San Diego with 60+ employees.
MPT models are all open source and open data and licensed for commercial use.
They maintain sever open source Python projects: https://github.com/mosaicml/.
They also allow you to train your own model and run it on your own compute environment.

### [Models](https://www.mosaicml.com/blog/llama2-inference)

Mosaic ML has 70B parameter embedding and conversational AI and code-generation versions of their LLM.
They claim that their Llama2-chat model is preferred to ChatGPT and Palm-Bison by human raters.

## 5. Octo.AI

##### Pricing

Llama2 and Mistral instruct are free. 
Your custom 7B model is .00001/token.
code generation is .0001/token.

## 5. Replicate.com

#### Pricing

You pay for the compute resources you use.

| Hardware | Price | GPU | CPU | GPU RAM | RAM |
| --- | --- | --- | --- | --- | --- |
| CPU | $0.000100/sec  
($0.36/hr) | \- | 4x | \- | 8GB |
| Nvidia T4 GPU | $0.000225/sec  
($0.81/hr) | 1x | 4x | 16GB | 8GB |
| Nvidia A40 GPU | $0.000575/sec  
($2.07/hr) | 1x | 4x | 48GB | 16GB |
| Nvidia A40 (Large) GPU | $0.000725/sec  
($2.61/hr) | 1x | 10x | 48GB | 72GB |
| Nvidia A100 (40GB) GPU | $0.001150/sec  
($4.14/hr) | 1x | 10x | 40GB | 72GB |
| Nvidia A100 (80GB) GPU | $0.001400/sec  
($5.04/hr) | 1x | 10x | 80GB | 144GB |
| 8x Nvidia A40 (Large) GPU | $0.005800/sec  
($20.88/hr) | 8x | 48x | 8x 48GB | 680GB |

