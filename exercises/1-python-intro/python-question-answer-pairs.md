# Getting started with Python

"You primarily write your code to communicate with other coders, and, to a lesser extent, to impose your will on the computer." -- Guido van Rossum [^1]

"Indeed, the ratio of time spent reading versus writing is well over 10 to 1. We are constantly reading old code as part of the effort to write new code. ...[Therefore,] making it easy to read makes it easier to write." [^2]

[^1]: [The Mind at Work: Guido van Rossum on how Python makes thinking in code easier](https://blog.dropbox.com/topics/work-culture/-the-mind-at-work--guido-van-rossum-on-how-python-makes-thinking) 

[^2]: _Clean Code_ by Robert C. Martin.


#### Question
What is the `this` package?

#### Answer

```python
import this
print(open(this.__file__).read())
```

#### Question 0
Name at least 5 ways to get help without using the internet.

```python
help()
?
??
print()
```

[tab]-completion ( autocomplete )
ctrl-R  ( search for previously typed commands )
up arrow ( scroll through previous commands )

#### Question 1
Which line(s) of a Traceback are the most important to read and understand when you get an error message?

#### Answer 1
The last two lines and the first two lines (in that order).

#### Question 2
What important information should you look for in an error message (Traceback)?

#### Answer 2
1. The line number of your code that failed
2. The error type
3. The error message (within the last line of code)

#### Question 3
What is the most insidious type of programming error?

#### Answer 3
When your code invisibly (quietly) does something to a variable or data that you did not intend.

#### Question 4
How can you find and correct it?

#### Answer 4
Examine your variables after every line of code to ensure what you intended to happen actually worked. And make sure there were no unintended "side effects".  Every Jupyter notebook cell that you create should have a single line of code followed by a bare variable name, so that you can examine the variable you intended to modify and confirm.

#### Question
Why is it so important to run jupyter notebooks top to bottom every single time you run them?

#### Answer
Because code is procedural, each cell is modifying the global names space. so one cell can undo or redo the work of another cell if you're not careful, creating duplicates or corrupted data.

#### Question
What is a good way to quickly try some lines of code out and explore a new package or module?

One line at a time in ipython (anaconda console) NOT Jupyter notebook

#### Question
How can you export the history of all your ipython code to the terminal or a file?

```ipython
history 
history -o -p
history -f filename.py
```

#### Question
How can you modularize your python code to make it more reliable and reusable?

Create functions and importable modules (`*.py` files)

#### Question
How can you automate repeated operations on data?

Create a `for` loop.

#### Question
What is DRY code?

Do not repeat yourself.
To accomplish this you have two software concepts you can use and two types of copy-pastes (repeating yourself) that they can help get rid of:

1. variables -- numbers or string that you've repeated
2. functions -- blocks of code with only one or two values changed

For (1), look for any numerical values or strings that are the same in two or more places in your code.
When you find them, DRY them up by creating a single global variable that you reuse everywhere those variables are in your code.

For (2), look for any blocks of code that are the same or similar to other code you've written or copy/pasted. 
Create a function for that block of code.
You can parameterize it (add named arguments or positional arguments) for any values that changed from one block of code to another.
Just like variable substitution before, you will substitute whole blocks of code with function calls for the new function you just created.

#### Question
How do you create a python module?

Text editor (like Sublime) to create a `.py` file.

#### Question
What is a good, free IDE or text editor for Python files?

vscode, atom, or sublime text (NOT Jupyter notebook and NOT windows Notepad)

#### Question
How do you import your own python modules into a jupyter notebook?

#### Question
Name at least 5 useful builtin functions.

#### Answer
```python
str()
repr()
len()
print()
sorted()
reversed()
```

#### Question
Name two python data types for storing a single number.

#### Answer
int
float


## everything is an object

```
dict.get()
duct.update()
__str__
__repr__
```

## built-in types

int
float

## builtin container for text

str

What does this output:
```python
text = "Hello"
text
```

```python
text[1]
```

```
"Hello"[1]
```

#### Question
Name at least 4 Python built-in container types.

```
str
list
set
dict
```

#### Question
What is the difference between `list.append()` and `list.extend()`?

```python
nums = [1, 2, 4]
nums

nums = nums.append(8)
nums

nums = nums.extend([16, 32])
nums
```

#### Question
What will `list("abc")` return?
What will `tuple("abc")` return?
What will `dict([[0,'a'], [1,'b'], [2,'c']])` return?


#### Question
Name at least 3 containers for numerical data. Hint, where do we usually store data for data science?

```python
pandas.DataFrame
pandas.Series
numpy.array
list
dict
collections.OrderedDict
collections.defaultdict
```

#### Question
What is the difference between a `numpy.array` and a `pandas.Series`
What do these output?

**np.array**
```python
import numpy
x = numpy.array([1,2,4,8])
x
```

**pd.Series**
```python
import pandas
s = pandas.Series(range(4))
s + pandas.Series(range(0, -4, -1))
```

#### Question
Name 4 python packages or modules (imports) that you use for STEM projects (Science, Math, Engineering).

```
math     # Built into Python
pandas   # Pandas
sklearn  # Scikit-Learn
numpy    # Numpy
```

#### Question
How do you create your own module with functions you can reuse on any project?

#### Question
Can you use the `as` keyword to rename a package?
What about renaming a package name using the assignent operator (`=`)?

#### Answer
```
import pandas
pd = pandas
```

#### Question
How can you find the source code for a python module or package?

```ipython
this??
this.__file__
```

#### Question
What does the `__name__` attribute of a module (py file) contain?
What does it contain when you run a python script rather than importing it as a module?

#### Answer
```ipython
>>> import collections
>>> collections.__name__

>>> %run printname.py  # file should contain `print(__name__)`
```

OR

```sh
$ python printname.py  # file should contain `print(__name__)`
```

#### Question
What are two ways to change the namd of the Pandas package to pd temporarily within a script?

#### Answer
```python
import pandas as pd
```

OR

```python
import pandas
pd = pandas
```

#### Question
How could you create your own module to contain your python code snippets that answer these questions.

#### Answer
Create a `.py` file that contains a function for each question.

#### Question

Which one of these are methods and which ones are functions? 
1. `read_csv()`
2. `to_csv()`
3. `pd.read_csv()`
4. `df.to_csv()`
5. `x = list([1]).append(2)`
6. `x = list([3]).extend([4])`

BONUS: Which one will return None?
Feel free to write your own python snippets to examine each object to find out its `type()`.

**1.**
```python
from pandas import read_csv

df = pd.read_csv(fn, index_col=0)
```

**2.**
```python
from pandas import DataFrame

DataFrame([1,2,3]).to_csv('filename.csv')
```

#### Question
What is the difference between `str` and `repr`?
Which methods on your objects do they call?

#### Question
What is the difference between `class` and `def`?
Which one must always have a `__call__` method?
How do you construct or instantiate an object from a `class`?

#### Question
What is the value of `np.nan > 0`?
Is it `True`, `False` or `None`?

#### Random notes

```text
x vs X hobbgoblin of little minds
shape == (16, 16, 1) grayscale, color

vars dir

.__dict__[attr] vs .attr

list comprehension with conditional filter and conditional values

lambda functions and apply

generator
```

## Advanced ideas/notes

* method vs function
* list of dictionaries vs nested dictionaries data
* `&` vs `and`
* `str` vs `repr`
* `class` vs `def`
* callable __call__ Class()
* `np.nan > 0`
* `x` vs `X`;  `dict` capitalization vs `OrderedDict` (hobbgoblin of little minds)
* shape == (16, 16, 1) grayscale, color image data
* vars dir
* *`.__dict__[attr]` vs `.attr`
* list comprehension with conditional filter and conditional values
* `lambda` functions and `df.apply`
* `generator` and `iter`
* Pandas objects with the `&` operator and `|` operator and conditionals
* indexing and slicing pandas objects with conditionals, lists of integers, and strings
* `df = pd.read_csv(fn, index_col=0)` and `df.to_csv(fn, index=False)`

### Resources

https://realpython.com/quizzes/

https://bogotobogo.com/python/python_interview_questions.php

https://www.educative.io/courses/learn-python-3-from-scratch

https://opensource.com/article/20/9/teach-python-jupyter

https://gist.github.com/lheagy/f216db7220713329eb3fc1c2cd3c7826

### Datasets

https://simple.m.wikipedia.org/wiki/List_of_countries_by_area

https://en.m.wikipedia.org/wiki/Lists_of_countries_and_territories

https://en.m.wikipedia.org/wiki/World_Happiness_Report
