# VSCodium

([VSCodium](https://vscodium.com/)) is an open source GUI IDE (integrated development) that runs anywhere you can run node.js.
When Microsoft bought GitHub they began their effort to exploit, extend and corrupt the VSCode open source project.
VSCodium is the pushback from the open source community to regain control of the releases that developers install on their machines.
It strips out all of the Microsoft "phone home" and proprietary dark-pattern features of VS Code.

So I'm retraining my fingers to learn VSCodium shortcuts instead of Sublime, because VSCodium is more open and has a more active ecosystem of plugins.
It works well with HTML5, Jupyter Notebooks, and adoc files by giving you rendered views of the text.
And this interface is getting incorporated into many online IDEs platforms such as [gitpod](./../1-gitpod).
