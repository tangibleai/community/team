# Local PyPi mirror

PyPi.org only works if you have a reliable and fast internet connection internet connection.
Creating a mirror on your laptop or your local network or your virtual private cloud or even just within the same region in your cloud provider can drammatically speed up your CI-CD pipelines and local development.

## References
- https://devpi.net/docs/devpi/devpi/stable/+d/quickstart-pypimirror.html#installing-devpi-server


```bash
export CONFIG_PATH=~/code/devpi
mkdir -p $CONFIG_PATH
cd $CONFIG_PATH
pip install -U devpi-server devpi-web supervisor
devpi-init
devpi-gen-config  # <1>
supervisord -c $CONFIG_PATH/gen-config/supervisord.conf
```
_<1> You can ignore the warning message about using a config file. This command is generating a new config file for you._

You should now have a local PyPi mirror running on your laptop at [localhost:3141](http://localhost:3141/root/pypi/+simple/)
and your new PyPi index URL is: `http://localhost:3141/root/pypi/`

You should now be able to install most packages with your local mirror:

```bash
pip install -i http://localhost:3141/root/pypi/+simple/ django
```

### TODO

You may want to install it permanently before you make it your default pypi index:

```bash
SERVER_CONF=~/.config/devpi-server
mkdir -p $SERVER_CONF
SERVER_CONF=$SERVER_CONF/devpi-server.yml
echo 'devpi-server:' >> $SERVER_CONF
echo '  serverdir: /var/db/devpi-server' >> $SERVER_CONF
echo '  secretfile: /etc/devpi-server/secret' >> $SERVER_CONF
echo '  host: localhost' >> $SERVER_CONF
echo '  port: 8080' >> $SERVER_CONF
```

So if you want to install a large package such as django you can now use your new local mirror index:

```bash
pip install --index http://localhost:3141/root/pypi django
```

You can even search the index:

```bash
pip search --index http://localhost:3141/root/pypi/ tangibleai
```

If you like what you see, you can make your local mirror the default pypi index for all your pip installs:

```bash
export PIP_DIR=~/.pip/
mkdir -p $PIP_DIR
export PIP_CONF=$PIP_DIR/pip.conf
echo '' >> $PIP_DIR/pip.conf
echo '# SEE https://devpi.net/docs/' >> $PIP_CONF
echo '[global]' >> $PIP_CONF
echo 'index-url = http://localhost:3141/root/pypi/+simple/' >> $PIP_CONF
echo '' >> $PIP_CONF
echo '[search]' >> $PIP_CONF
echo 'index = http://localhost:3141/root/pypi/' >> $PIP_CONF
```

