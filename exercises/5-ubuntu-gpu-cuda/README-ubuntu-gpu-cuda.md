# GPU, Nvidia, Cuda, PyTorch versions

#### References
* [Stack Overflow for checking GPU availabiltiy](https://stackoverflow.com/a/48152675/623735)
* Ask Ubuntu for installing CUDA [2021 Answer](https://askubuntu.com/a/1077063/31658)
[latest CUDA release notes about NVIDIA driver compatibility](https://docs.nvidia.com/cuda/cuda-toolkit-release-notes/index.html) gave me: 530.30.02
* apt (deb) packages for [Ubuntu 22.04]( 
https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/)
* apt (deb) public [signing key for Ubuntu 22.04]( 
https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/3bf863cc.pub)

#### *`lspci | grep NVIDIA`*
* GeForce GTX 1080 Ti (GP102) - compute 6.1 compatible
* GeForce GTX 980 Ti (GM200) - compute 5.2 compatible

#### latest NVIDIA Drivers
- NVIDIA driver v 530.30.02 for linux
- NVIDIA driver v 531.14 for Windows

#### *`apt search nvidia-driver`*
gave the v 525 driver being the latest

Fetch the keys and download the NVIDIA driver deb packages for Ubuntu 22.04:

```bash
sudo apt-key adv --fetch-key https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/3bf863cc.pub
sudo bash -c 'echo "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64 /" > /etc/apt/sources.list.d/cuda.list'
sudo bash -c 'echo "deb http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu2204/x86_64 /" > /etc/apt/sources.list.d/cuda_learn.list'
sudo apt update
sudo apt install cuda-12-1
sudo apt install libcudnn*
```

#### *`nvcc --version`*
```txt
nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2023 NVIDIA Corporation
Built on Tue_Feb__7_19:32:13_PST_2023
Cuda compilation tools, release 12.1, V12.1.66
Build cuda_12.1.r12.1/compiler.32415258_0
```

```bash
gupi@gupi-tow:~$ /sbin/ldconfig -N -v $(sed 's/:/ /' <<< $LD_LIBRARY_PATH) 2>/dev/null | grep libcudnn
libcudnn_adv_infer.so.8 -> libcudnn_adv_infer.so.8.8.1
libcudnn_adv_train.so.8 -> libcudnn_adv_train.so.8.8.1
libcudnn_cnn_infer.so.8 -> libcudnn_cnn_infer.so.8.8.1
libcudnn_cnn_train.so.8 -> libcudnn_cnn_train.so.8.8.1
libcudnn.so.8 -> libcudnn.so.8.8.1
libcudnn_ops_train.so.8 -> libcudnn_ops_train.so.8.8.1
libcudnn_ops_infer.so.8 -> libcudnn_ops_infer.so.8.8.1
```

```bash
terrance@terrance-ubuntu:~$ nvidia-smi
+---------------------------------------------------------------------------------------+
| NVIDIA-SMI 530.30.02              Driver Version: 530.30.02    CUDA Version: 12.1     |
|-----------------------------------------+----------------------+----------------------+
| GPU  Name                  Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf            Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                                         |                      |               MIG M. |
|=========================================+======================+======================|
|   0  NVIDIA GeForce GTX 980 Ti       On | 00000000:03:00.0 Off |                  N/A |
|  0%   38C    P8               13W / 250W|     15MiB /  6144MiB |      0%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+
|   1  NVIDIA GeForce GTX 1080 Ti      On | 00000000:04:00.0 Off |                  N/A |
| 23%   30C    P8                9W / 250W|      6MiB / 11264MiB |      0%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+
                                                                                         
+---------------------------------------------------------------------------------------+
| Processes:                                                                            |
|  GPU   GI   CI        PID   Type   Process name                            GPU Memory |
|        ID   ID                                                             Usage      |
|=======================================================================================|
|    0   N/A  N/A      4354      G   /usr/lib/xorg/Xorg                           11MiB |
|    1   N/A  N/A      4354      G   /usr/lib/xorg/Xorg                            4MiB |
+---------------------------------------------------------------------------------------+

```

```bash
python -c "import torch; print(torch.cuda.is_available())"
```

```python
>>> import torch
>>> torch.cuda.is_available()
True
>>> torch.cuda.device_count()
2
>>> torch.cuda.current_device()
0
>>> torch.cuda.device(0)
<torch.cuda.device at 0x...>
>>> torch.cuda.get_device_name(0)
'GeForce GTX ...'
```
