# Python Programming

## Online Courses and Exercises

* [Python miniprojects and exercises](https://gitlab.com/tangibleai/team/-/tree/main/exercises/) in this repository
* Guru99.com [Python Tutorials](https://www.guru99.com/python-tutorials.html)
* [Fundamentals of Python](https://runestone.academy/ns/books/published/fopp/index.html?mode=browsing) - free interactive textbook by Bob
* [Runestone Interactive](http://runestoneinteractive.org) **open source** project
* _Fundamentals of Python_ [book](http://www.openbookproject.net/thinkcs/python/english2e/) by Jeffrey Elkner, Allen B. Downey, and Chris Meyers
* [How to Think Like a Computer Scientist](https://runestone.academy/runestone/static/thinkcspy/index.html) interactive book by Brad Miller, David Ranum 
* [Programs Information and People](https://runestone.academy/runestone/static/pip2/index.html) by Paul Resnick   
* Activecode framework [Skulpt.org](http://skulpt.org)
* Codelens debugger and [Online Python Tutor](http://www.pythontutor.com)
* Georgia Tech [CSLearning4U research group](http://home.cc.gatech.edu/csl/CSLearning4U)

## Skills Checklist

* [ ] importing python packages: `this`, `os`, `sys`, `sys.path_append()`
* [ ] finding, installing, importing python packages: `sklearn`, `Pandas`, `tqdm`
* [ ] reading Tracebacks and error messages
* [ ] conditional expressions: `if`, `else`, `>`, `<`, `==`, `!=`, `not`, `in` 
* [ ] loops: `for`, `while`, `enumerate`, `zip`, `zip*`, `tqdm`
* [ ] functions: args, kwargs, `def`, `return`
* [ ] classes: `class`, methods, attributes, `__init__`, "everything is an object"
* [ ] scalar numerical data types: `int`, `float`
* [ ] sequence data types: `str`, `bytes`, `list`, `tuple`
* [ ] mapping: `dict`, `Counter`
* [ ] type coercion: `list(str)`, `dict([(1,2)]`, `dict(zip(range(4), 'abcd'))`
* [ ] sets: `set`
* [ ] getting help: `help()`, `?`, `??`, `[TAB] completion`, `vars()`, `dir()`, `type()`, `print()`, `.__doc__`)
* [ ] files: `with`, `open`, `write` 
* [ ] manipulate files with bash: `!`, `pwd`, `mkdir ~/code`, `mv`, `cp`, `touch`
* [ ] navigate directories: `cd`, `pwd`, `ls -hal`, `/`, `~`, `*`, `$HOME`

These are called doctests. The question is the python code the line prefixed with `>>>`. The correct answer is the value you would put on the line after the python code based on what that expression would output.

```python
>>> dict(enumerate('Hello'))[1]
'e'
```

```python
>>> dict(enumerate('Hello'))[:-1]
TypeError: unhashable type: 'slice'
```

```python
>>> set('Oh Hello'.lower()) - set('Ohio ')
{'e', 'l'}
```

```python
>>> dict(Counter(list('ABBA'))
Counter({'A': 2, 'B': 2})
```

```python
>>> dict(zip(*['RD', '2'*2]))
{'R': '2', 'D': '2'}
```

```python
>>> pwd
>>> set(Path('.').absolute().__str__()) - set(_)
set()
```

```python
>>> def f(x):
...     while x > 2:
...         x -= 2
...     return x
>>> f(128)
2
```

```python
>>> 'lower' in dir('ect')
True
```

```python
>>> 'dir' in list('direct')
False
```

```python
>>> [1, 2, 3] * 2
[1, 2, 3, 1, 2, 3]
```

```python
>>> np.array([1, 2, 3]) * 2
[2, 4, 6]
```

```python
>>> pd.Series([1, 2, 3]) - 1
0    0
1    1
2    2
dtype: int64
```

```python
>>> pd.Series([1, 2, 3]) + pd.Series([1, 2, 3], index=[1, 2, 3])
0    NaN
1    3.0
2    5.0
3    NaN
```

### Resources

https://realpython.com/quizzes/

https://bogotobogo.com/python/python_interview_questions.php

https://www.educative.io/courses/learn-python-3-from-scratch

https://opensource.com/article/20/9/teach-python-jupyter

https://gist.github.com/lheagy/f216db7220713329eb3fc1c2cd3c7826

[Using jupyter notebooks for teaching](https://jupyter4edu.github.io/jupyter-edu-book/catalogue.html)

## basics

### 1

Name at least 5 ways to get help without using the internet.

```python
help()
?
??
print()
# [tab]-completion ( autocomplete )
# ctrl-R  ( search for previously typed commands )
# up arrow ( scroll through previous commands )
```

### 2

Which line(s) of a Traceback are the most important to read and understand when you get an error message?

1. the last two lines
2. the first two lines

### 3

What important information should you look for in an error message (Traceback)?

1. the line number of your code that failed
2. the error type

### 4

What is the most insidious type of programming error?

When your code invisibly (quietly) does something to your data that you did not intend.

### 5

How can you find and correct insidious (unknown unknown) errors?

Examine your variables after every line of code to ensure what you intended to happen actually worked. And make sure there were no unintended "side effects".  Every Jupyter notebook cell that you create should have a single line of code followed by a bare variable name, so that you can examine the variable you intended to modify and confirm.

### 6 

Why is it so important to run jupyter notebooks top to bottom every single time you run them?

because code is procedural, each cell is modifying the global names space. so one cell can undo or redo the work of another cell if you're not careful, creating duplicates or corrupted data.

### 7

What is a good way to quickly try some lines of code out and explore a new package or module?

One line at a time in ipython (Anaconda console) NOT Jupyter notebook

### 8

How can you export the history of all your ipython code to the terminal or a file?

history 
history -o -p
history -f filename.py

How can you modularize your python code to make it more reliable and reusable?

create functions

How can you automate repeated operations on data?

for loop

What is DRY code?

do not repeat yourself

How do you create a python module?

Text editor to create a py file.

What is a good IDE or text editor for Python files?

vscode, atom, or sublime text (NOT Jupyter notebook and NOT windows Notepad)

How do you import your own python modules into a jupyter notebook?



## built in functions

Name at least 5 useful builtin functions.

```python
str()
repr()
len()
print()
sorted()
reversed()
```

## built-in numerical types

Name two python data types for storing a single number.

int
float


## everything is an object

```
dict.get()
duct.update()
__str__
__repr__
```

## built-in types

int
float

## builtin container for text

str

What does this output:
```python
text = "Hello"
text
```

```python
text[1]
```

```
"Hello"[1]
```

## built-in container types

```
str
list
set
dict
```


nums = [1, 2, 4]
nums

nums = nums.append(8)
nums

nums = nums.extend([16, 32])
x 

 = list(''abc")

## external containers for numerical data

Name 3 containers for numerical data. Hint, where do we usually store data for data science?

```
pandas.DataFrame
pandas.Series
numpy.array
```

What does this output:


```
import numpy
x = numpy.array([1,2,4,8])
x
```

```
import pandas
s = pandas.Series(range(4))
s + pandas.Series(range(0, -4, -1))
```


## modules

Name 4 python packages or modules (imports) that you use in data science.


math
Pandas
Sci
create your own module
