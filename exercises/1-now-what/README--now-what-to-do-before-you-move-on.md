# Now what?

## Before you Go

Before you move on to bigger and better things, here's a checklist of things to help you wrap up. Your future self will appreciate it.

- [ ] 1. Push your latest project code up to a gitlab repository
- [ ] 2. Push all the data required for your project to your repo
- [ ] 3. Edit the tangibleai/team [proai.org/project-reports](proai.org/project-reports) markdown file to include a link to your report or GitLab repo
- [ ] 4. Create a merge request to  `gitlab.com/tangibleai/team` with your final report (.md, .docx, .pptx, or .ipynb)
- [ ] 5. Add your gitlab repo URL to the gitlab.com/tangibleai/team project ideas markdown file
- [ ] 6. Think about your future self coming back to your project after you've forgotten all about it -- document (in README.md) what you will need to know to get started working on your code (run it, test it)
- [ ] 7. Edit or add an idea to the the [proai.org/project-ideas](proai.org/project-ideas) markdown file (with a merge request) so future interns can build on your work)
- [ ] 8. Revisit the skills checklist to see what you've learned and get ideas on where to go from here!

So you wrapped up a project and completed some exercises. Now what?

## Projects

If you are looking for another passion project, you can continue working on your internship project. 
You probably created a "future work" section. If not do that now to record all your ideas, for your future self.

If you want to shift gears and work on something new, you can look back the the [project ideas list](proai.org/project-ideas) for inspiration. 

And here are some other resources to give you project ideas:

- [ProAI's takehome quizzes]()
- eulerproject.org
- Kaggle
- paperswithcode.com
- any chapter of _Artificial Intelligence: A Modern Approach_

## Courses

If you're looking for something more organized, like a course, you might check out one of these:

- Coursera: Some free college and graduate-level courses
- Udacity: Mostly paid courses
- Udemy
- opencourseware: Free classroom material from top notch MIT & Stanford courses

## Social

Be prosocial with your learning. Support people on open, ad-free, social networks.

- There's an ActivityPub network [for pretty much anything](https://awesomeopensource.com/projects/activitypub), book reviews, professional life, hobbies, etc: 
- Mastadon (microblog like Twitter only more prosocial)
- Reddit: some [subreddits](proai.org/prosocial-subreddits) are somewhat prosocial (https://gitlab.com/tangibleai/team/-/blob/master/exercises/1-prosocial-ai/prosocial-subreddits-and-users.md)
- Substack blogs that pay the writer directly (and no ads)
