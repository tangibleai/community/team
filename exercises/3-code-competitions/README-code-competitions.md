# Coding Competitions:

### LLM code generation benchmarks

Smart approach to measuring LLM code-generation accuracy using metric "ACC{k-correct}#{in-top-n}" (https://arxiv.org/pdf/2312.02143.pdf) and gave this plot:

[![negative correlation with difficulty and competition date](./GPT-4-code-generation-performance-only-easy-problems-before-2021.png)](./GPT-4-code-generation-performance-only-easy-problems-before-2021.png)

DeepSeek code seems to be the best, but it's only a few percentage points better than CodeLlama


### Human coding competitions
- [Google Code Jam](https://codingcompetitions.withgoogle.com/codejam) $15k award
- [Google Hash Code](https://codingcompetitions.withgoogle.com/hashcode) $4k award
- [Google Kick Start](https://codingcompetitions.withgoogle.com/kickstart) [geeksforgeeks prep guide](https://www.geeksforgeeks.org/prepare-google-kickstart/)
- International Collegiate Coding Competition (ICPC.global): $15k award [rules](https://icpc.global/worldfinals/rules#) [geeksforgeeks guide](https://www.geeksforgeeks.org/how-to-prepare-for-acm-icpc/) - popular with Middle East and India colleges. 
- [Facebook Hacker Cup](https://www.geeksforgeeks.org/facebook-hacker-cup/) cash prizes for 25 finalists
- [Microsoft Imagine Cup](https://imaginecup.microsoft.com/en-us/Events) 
- [CodeForces.com](https://codeforces.com) [geeksforgeeks guide](https://www.geeksforgeeks.org/10-best-tips-to-get-started-with-codeforces/) Timed Data Structures and Algorithms problems -- with datasets of past compeitions for LLM training? run by ton.org and imfo.ru (ITMO in Russian)
- [Code Generation Datasets for LLMs](https://paperswithcode.com/datasets?q=Enterprise-Driven+Open+Source+Software&task=code-generation&page=1)
- [Alpha Code training set](https://github.com/google-deepmind/code_contests)
- Aizu [https://judge.u-aizu.ac.jp](https://judge.u-aizu.ac.jp) [CodeNet](https://github.com/IBM/Project_CodeNet) |
- AtCoder [https://atcoder.jp](https://atcoder.jp) [CodeNet](https://github.com/IBM/Project_CodeNet) |
- CodeChef [https://www.codechef.com](https://www.codechef.com) [description2code](https://github.com/ethancaballero/description2code) |
- Codeforces [https://codeforces.com](https://codeforces.com) [description2code](https://github.com/ethancaballero/description2code) and Codeforces |
- HackerEarth [https://www.hackerearth.com](https://www.hackerearth.com) | [description2code](https://github.com/ethancaballero/description2code) |
- [Aizu](https://judge.u-aizu.ac.jp) by CodeNet used for Alpha Code
- [AtCoder](https://atcoder.jp) by CodeNet used for Alpha Code
- [CodeChef](https://www.codechef.com)    description2code
Codeforces  https://codeforces.com  description2code and Codeforces
HackerEarth     https://www.hackerearth.com     description2code
- [GeeksforGeeks.org **practice portal**](https://practice.geeksforgeeks.org/)
- [GeeksforGeeks.org **Monthly Job-a-thon**](https://practice.geeksforgeeks.org/events/rec/job-a-thon?utm_source=gfgpractice&utm_medium=events_page&utm_campaign=events_series_jobathon) job seekers
- [Bi-Wizard Coding](https://practice.geeksforgeeks.org/events/rec/step-up-coding-school?utm_source=gfgpractice&utm_medium=events_page&utm_campaign=stepupcoding_school) exclusively for students
- [Aim GATE](https://practice.geeksforgeeks.org/events/rec/aim-for-gate?utm_source=gfgpractice&utm_medium=events_page&utm_campaign=aim_for_gate)
- [Interview Series](https://practice.geeksforgeeks.org/events/rec/interview-series?utm_source=gfgpractice&utm_medium=events_page&utm_campaign=events_series_interview_series) - weekly challenges to prep for interviews
- [Problem of the Day](https://practice.geeksforgeeks.org/problem-of-the-day?utm_source=gfgpractice&utm_medium=events_page&utm_campaign=events_series_problem_of_the_day) - daily DSA problem
- https://leetcode.com/ - leader board with score/rank
- https://ioinformatics.org/

