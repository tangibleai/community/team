# MOOC Teaching

If you like to show others how to do the things you've learned, you might enjoy doing your teaching within a MOOC.
This will increase your reach and maybe even give you a little passive income.
Here are some of the most open learning management platforms.

```yaml
- "[p2pu](https://www.p2pu.org/en/services/)"
    software: [course-in-a-box](https://github.com/p2pu/course-in-a-box)
    business: nonprofit, teachers are volunteers
- "[edx](https://edx.org)"
  - repo: https://github.com/edx/
  - software: [Open edX platform](https://openedx.org)
- "[Udemy](udemy.com) - large number of paying students"
- "[Youtube Channel for Kubernetes](https://www.youtube.com/watch?v=X48VuDVv0do)"
  - repo: udemy downloader - https://github.com/r0oth3x49/udemy-dl
  - example: "https://www.youtube.com/watch?v=X48VuDVv0do (kubernetes by [TechWorldwithNana](https://www.youtube.com/@TechWorldwithNana))""
```