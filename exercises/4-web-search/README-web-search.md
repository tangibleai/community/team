# curl to load data

### references:
- https://meilisearch-wi9t.onrender.com/
- https://gitlab.com/tangibleai/public/meilisearch
- https://gitlab.com/tangibleai/community/team/-/tree/main/exercises/4-web-search
- https://gitlab.com/tangibleai/community/team/-/tree/main/exercises/2-crawl-wikipedia

## [Install with bash](https://docs.meilisearch.com/learn/getting_started/quick_start.html#local-installation)


### cURL
Under the local installation section there are several Tabs where there is a cURL tab.
This curl command downloads an installation shell script from install.meilisearch.com.
It immediately streams it into the shell interpreter (`bash`) to install meilisearch locally.

```bash
curl -L https://install.meilisearch.com | sh
```

### Docker
Click on the docker tab to see the instructions for using Docker to build and install meilisearch.
This is what worked successfully for me on Render.com, I think.
I created a docker image on Render.
Then I put these two docker commands in the Settings for that render instance (image name and entrypoint):

```bash
# Fetch the latest version of Meilisearch image from DockerHub
docker pull getmeili/meilisearch:v0.30

# Launch Meilisearch in development mode with a master key
docker run -it --rm \
    -p 7700:7700 \
    -e MEILI_MASTER_KEY='MASTER_KEY'\
    -v $(pwd)/meili_data:/meili_data \
    getmeili/meilisearch:v0.30 \
    meilisearch --env="development"
```

# Launch Meilisearch
./meilisearch


## find and verify the MASTER_KEY
curl \
  -X GET 'http://localhost:7700/keys' \
  -H 'Authorization: Bearer MASTER_KEY'


```ash
export MEILI_MASTER_KEY='<CHANGEME>....M9Htlq99&WwsLZ0Pe58#k...</CHANGEME>'
```

## Failed
```ash
curl \
  -X POST "http://localhost:7700/indexes/movies/documents?apiKey=$MEILI_MASTER_KEY" \
  -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer apiKey' \
  --data-binary @movies.json
# {"message":"The provided API key is invalid.","code":"invalid_api_key","type":"auth","link":"https://docs.meilisearch.com/errors#invalid-api-key"}
```

```ash
curl \
  -X POST "http://localhost:7700/indexes/movies/documents?primaryKey=id&apiKey=$MEILI_MASTER_KEY" \
  -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer apiKey' \
  --data-binary @movies.json
# {"message":"The provided API key is invalid.","code":"invalid_api_key","type":"auth","link":"https://docs.meilisearch.com/errors#invalid-api-key"}
`
