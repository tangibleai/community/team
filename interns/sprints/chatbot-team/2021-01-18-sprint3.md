Sprint Plan 1: 

Maria:
- Maya Bot: All the stuff from previous Sprint
  - Show Maya how free text logging works 
- Maya Bot: Plan the handoff for Maya Bot 
- Maya Bot: Schedule with Ankit's replacement 
- Regulationship: re-record demo of Botpress for clients, push to close the deal 
- Share calendar


Maya
- Maya Bot: Finish connecting new intents in English and Nepali      **In progress** 
- Maya Bot: Record tutorial videos: analytics logging, flow interrupt
- Maya Bot: Map out free text 
- Maya Bot: Revise intro to stories
- Regulationship: Build small flow on Botpress to get used to it
- Maya Bot: Find a QA professional on fiverr and outsource the manual QA test script for Maya 


Backlog:
- Implement Chatbase Integration   
- Fix NLU (put language recognition first)
