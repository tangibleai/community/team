# CoSips

- "join me" or "Cosipping here" sticky notes with QR code and shorturl listing partner cafes, sortable by things like distance, bike racks, prosocialness, etc, secretsips, and instructions on how not to interrupt someone working there if the sticknote is rightside up, but interrupting them if it is missing or upside down to ask "may I cosip with you?" or "may I sit here?"
- co means coop, cowork, cooperate, community, even commune and coordinate and communist (could be bright red and black logo, to be eye catching and trigger decisiveness/curiosity about logo and url)
- use location/ip info to prioritize cafes nearby on home page
- have cafe-specific urls/slugs QR codes for cafes that cooperate, with things like wifi passwords (so that staff isn't bothered)
- cosipper marketing language and web copy can address curmudgeon who likes to work uninterrupted and alone by making sure that baristas and waiters do not ask them if they've purchased anything
- insider tips like "cosippers that work late at Subterranean in North Part often bring in the patio chair to get a cross fit workout. body builders even bring in a few tables, practiving their balance and form as they carefully pass through the glass doors with the heavy metal tables and chairs"
- off-menu food items
- happy hour prices
- discount punch cards
- coupons for coworkers
- prosocial reciprocity language around purchasing something every 2-3 hours depending on how generous you are
- ratings of WiFi networks for security and bandwidth and reliability (talk to Leo about his database of San Diego cafe Wifi tests)
- information on landlords, their values and wealth (to adjust tips accordingly)
- information on employee salaries (tipping more at cafes where wages are high relative to neighborhood wages)
- promoting prosocial family-owned, non-chain cafes
- SEO that hacks google and google Maps rankings for prosocial cafes
- encouraging yelp reviews for prosocial cafes/restaurants that allow cosippers
- "[x] namesquatted some domain names cosipz.com cosipz.org cosips.org" 

