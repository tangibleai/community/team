# Training Wheels

Projects for new interns. Goal is to build a "wheel"... a pip-installable python module.

Tutorial or example webapp might come from these sources (in increasing difficulty):

1. Cory Shaefer Django Blog tutorial
2. Tanbot web application
3. Stigsite+discord logging Django app
4. Stigsite+zulip monitoring Django app

Django apps can target any of the following applications (in increasing difficulty):

- Manzana - project brainstorming and planning by Rochdi
- NLPiA.org - Natural Language Processing in Action blog and resources (exmaple apps)
- MAItag - REST API endpoing for intent classification based on django-mathtext
- ProAI.org/MAIteam/Tanbot - internship welcome letters, contracts, resources, exercises (from team repo)
- MAIteam - education and onboarding forum (simple LMS)
- qary-search - Document upload, search, and QA - postgres full text + vector search backend (supabase)
- Bookwyrm - Book club forum, reading diary, reading list (goodreads) 
- Nudger (simplified) - faceofqary talking to REST API
- Activity Pub Search - mastodon and Substack scraping and search
- URL shortener endpoint and web interface
- Stigsite - Discord/slack logging/monitoring/search
- Tangibleai.com - social impact business home page and blog
