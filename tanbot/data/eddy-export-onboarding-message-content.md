# Onboarding Plan
maria

##### How to use this document
Eddy is your tour guide for Onboarding. This download contains the full contents of your Onboarding Plan, which can be useful in planning and documentation management, as a complement to Eddy's guided Slack-native program. Keep in mind that this document represents your plan on the day and time it was downloaded, and will not reflect updates made after that date.

### Onboarding_Intro
Hi there! 👋 Let's get started with your Onboarding.
It's important to be able to connect your work to your team's work. Your first week here can set the stage for the rest of your career here. I want to make sure you start on the right foot.
Here are some commands that will help you in your onboarding.
*checkin*: Manually trigger the check-in flow, in which I ask you if you've completed the tasks due for the day.
*onboard*: Move on to the next section once all your tasks are complete.
You can type *help* at any time to get a list of commands that I understand.
Task: Type *checkin* now, to complete your first task.

### Expectations_L1
First, I want to help set your expectations for working here. This information came from your teammates and Manager, and I hope it helps you figure out what is expected of you here.
Let's talk about what you contribute to this team. Your Manager sees your contribution to the team like this: Contribute to _qary_, an open-source cognitive assistant.
Task: It's important to be able to connect your work to your team's work. Long-term success in this role looks like: features and lines of code contributed to the qary repository. If you don't already have one set up, you should schedule a 1:1 with your manager to discuss how you contribute to the team's success. Make sure you schedule that today.
It will help you to know what a typical day looks like for someone in your role. Ask your Buddy or anyone else at Tangible AI, how much time they spend coding (including writing unit tests, fixing bugs, and researching online the best way to do things) vs reading and writing docs or having meetings with others on the team.

### Communications
Being new can be discombobulating. These resources will help!
You might have already figured out whether your teammates turn their video on. If you're not sure, Slack your Buddy and ask for their advice.
Task: Let's talk about the tools we use to get work done. We use these tools to communicate: Text, Phone, Email, Standup meetings, Adhoc video meetings, Ticketing tool, Slack. Do you have access to all these tools?
Task: I know there's a lot of information to digest when you're just getting started.  The most important Slack channels for you to pay attention to as you are: sdmachinelearning.slack.com: #qary tangibleai.slack.com: #general
Task: Standup meetings are an important part of how we share information and push our work forward. We have daily Standups. Make sure the next Standup meeting is on your calendar, and Slack your Manager if you don't have it.

### Team_L1
You'll need to know where to go and who to ask when you need help. Apart from me, obviously. I got you. 👯
It's OK if you get stuck during onboarding. There are people here to help! Your Manager says the best way to get help while you are onboarding is: reaching out to team members
Task: It's not just me helping you ramp up here - there's a whole team, including a Buddy, who will help you with any questions you don't feel like you can go to your Manager with. Your Buddy is <@UV366K3SA>. You should schedule a 1:1 with them today or tomorrow.
Task: Your Manager and your Buddy can tell you which team members you should prioritize meeting with first. Ask them and set up 1:1s as soon as you can.

### Expectations_L2
I'm here to help you make a good impression in your new role. This section is about how you show up here.
I want you to stand out for all the right reasons, to dress for success you should know what your teammates say about the office dress code. Your teammate says: We have no dress code. All employees work remotely. T-shirts and shorts are common.
At this point you've probably seen plenty of company-wide guidelines. Your team may also conform to an engineering code of conduct. You should ask you buddy or teammates if this exists at our company.
You need to understand how our team prioritizes work in order to know what the most important contribution is that you can make right now, and so you can offer input on the priority of future work. Here is some information from a colleague about how we prioritize work: Individual team members self-assign features on GitLab Issues (Features) board and discus with the team if they need help prioritizing their work. At sprint planning meetings we set top level priorities as a team.
Task: Our dev teams work in sprints, or short-term, intensive development cycles. Check your calendar for sprints you've already been added to. If you don't see them, message your Manager to be invited! 🏃

### Orientation_L1
Let's get to work! 💪 You are expected to make your first commit in about your first week. Don't worry - I'll help you through the whole process. Here are a few resources and tasks to get you started.
Your manager has hand-picked this project for your first commit: qary improvement. <https://gitlab.com/tangibleai/qary/-/issues>
Task: Let's look at the code repo. We keep code in GitLab.com. Your next task is to spend some time reviewing the code repo. Jot down some notes as you review so  you can ask questions during your 1:1s.
Task: Your next task is to set up your dev environment. You can read the guide here for setting that up: http://gitlab.com/tangibleai/qary/
OK, now we're going to learn about the SDLC our team is responsible for, All phases (all team members participate in our automated continuous integration process): requirements, design, implementation, testing, deployment, and maintenance..
Task: Take some time to read the documentation on our SDLC: No
Task: Our team keeps documentation in the following locations, you'll want to make sure you have access to all of them: Google Drive, Dropbox, gitlab.com After you get access, get a sense for how the knowledge is structured, look over the table of contents or content index and start to familiarize yourself with the different topic areas.

### Process_L1
Let's get you set up to make a pull request!
Let's talk about how we review code. Your teammate said this is how we do it: https://gitlab.com/tangibleai/qary/-/blob/master/docs/onboarding/code-reviews.md
We keep track of source code here: gitlab.com/
Let's learn about the CI/CD pipeline! Read this description to familiarize yourself with how we employ these practices: We use the `.gitlab-ci.yml` to configure automated tests and coverage reports (pushed to coverage.io for GUI interaction). Doctests are encouraged for simple functions. Integration tests are allowed in tests/test*.py . Automated deployment to google cloud is enabled on some repos. Most are deployed manually to Digital Ocean or released to pypi.org.
You should learn about how our team uses Git flows. This is how a teammate put it: `git flows` is not a thing, Atlassian has a product they call `gitflows`. We don't use it. We only use open source tools (gitlab).
Here's how your teammate describes the Pull Request process: Developer makes a merge request from their branch on gitlab.com to master.
Task: Your next task is to create your first pull request. Here is some additional documentation on this topic: A merge request template is launched when the merge request in initiated.

### Protocol_and_First_Commit

OK, next let's review the libraries and frameworks you'll use in your work. Ask your Buddy what frameworks and libraries we use the most. If you're not familiar with some of them, set aside time to do some research!
Methodology is about more than workflows, it's about how we work as a team. Ask your Buddy if we use agile, scrum, or another style.
Search the documentation store for info on how we work, and if you're not familiar with the methodology, it's time to bone up.
Let's learn about our best practices for Git branching and merging. Ask a colleague to walk you through.
Task: I'm sure you've been making progress on your first commit. I'll check in with you later today to see how your project is going.

### Product_Functions
Our product solves problems for people. The following info will help you understand the 'why' behind your work.
Now that you know more about the product we build, you should learn about it's primary functions. You teammate describes it like this: http://gitlab.com/tangibleai/qary and http://tangibleai.com/
It's important for you to know all the ways customers can access our product.  It is available on the following platforms: Linux, Windows, and Mac
Every team has its own shorthand of acronyms and shortened words. Learning all the acronyms and terms we use can be confusing, here's a link to our internal glossary so you can start to figure out what we are all talking about. https://gitlab.com/tangibleai/qary/-/tree/master/qary/data/faq
Task: OK, on to what problems the product solves. You teammate says this is what the product does: Saves the world from manipulative, deceptive virtual assistants and bots.. Spend some time today learning about how our product addresses specific customers challenges.

### Orientation_L2
At a high level, here's how we code.
Task: Every company has conventions for how they like to code. We're no different! Our documentation on coding conventions is here: We comply with PEP8 (https://www.python.org/dev/peps/pep-0008/#code-lay-out) and utilize the Napolean (Google) docstring style (https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html). All functions and classes should have at least a single-line Docstring. Most have doctest examples. Most of us use the Sublime Text plugin package 'Anaconda' to automatically correct formatting errors ( https://gitlab.com/tangibleai/qary/-/blob/master/docs/developer-guides/Anaconda.sublime-settings )
You can find Best Practices here: Minimize code complexity by breaking complex functions into smaller functions. Reuse others' code whenever possible. Avoid premature optimization. Pursue document-driven development (including a doc-test), using an issue description from gitlab as the first draft of documentation for a function  or class. Create a feature branch for each new issue/feature you work on (prefixed with the name 'feature-' or 'bugfix-'). Commit several times a day. Push at least daily. Issue merge requests with the WIP prefix until they are ready for review. You are allowed to merge to master only after another reviewer has approved your merge request.
And you'll need to know how we comment in code. Here's what your colleague said: Minimize comments and prefer longer explicit function and class and variable names that make comments unnecessary.
Task: Today you should explore the front end of our product. It's built on React.
Task: Let's talk about the back end of our product. Its tech stack is: Django, Python, Elastic Search, PostGRESQL, Bash (deployment).

### Protocol_Security
We take security seriously. Here are some resources about how we manage it.
Now, let's get familiar with the security protocols that we follow for development. You can find an overview of our security protocols here: Just don't add passwords and tokens to git repositories ;)
We take code cleanliness seriously around here. For linting code, we use these processes and tools: flake8: https://gitlab.com/tangibleai/qary/-/blob/master/setup.cfg#L179
Good versioning keeps us on track. Your teammate described our versioning protocol like this: releases and version bumps are managed by the CTO
Task: Let's focus on dev team methodologies. We use Test-driven development, documentation driven development (python doctests) in our work. If you're not already familiar with it, take time today to do your own research.

### Product_Shipping_QA
I don't know about you but I think shipping days are just about my favorite days! 🎉 Here's how we do QA, acceptance, and what success looks like for shipping.
Today you should learn what success looks like for shipping and releasing products and features. This is how your Manager described it: Number of forks, clones, and gitlab repo likes.
Understanding what 'done' looks like ensures that we deliver a quality product and helps us avoid rework. Your teammate defined done in this way: Someone else on the team approves a merge request linked to a ticket
You can avoid extra work and delivery delays by making sure you thoroughly understand the requirements of a given task. Our acceptance criteria for stories is documented here: Self-select and self-approve stories and tickets. Priorities are set at monthly sprint planning meetings.
Task: QA makes sure our customers get a great experience, no matter how they use our product. Schedule a 1:1 with your QA team member to figure out how it's done here. Ask your Buddy if you don't know who that is.

### Process_Tickets_and_Support
This is how ticketing and supporting other teams works here. 🎟
Our team relies on other teams in order to get work done. You can probably see examples of how other teams support your work. If not, ask someone in your team for some examples.
Ask someone on your team about how your work supports others in the organization.
Knowing how tickets are handled will give you a headstart on your onboarding. If you haven't already figured out the ticketing system, shout out to your Buddy to find out.
Look for documentation on our ticketing system. You need to feel confident that you know how it works.
Task: It's important you understand the process for submitting a ticket.  Ask your Buddy to demonstrate creating a ticket. Then, create your own practice ticket and submit it.
Task: If you haven't tackled a bug yet, approach a team member you haven't spoken to much and ask if there is documentation or process for bug best practices. Next, go looking for a low-priority bug to work on today.

### Product_General
We strive to make amazing, helpful things. Today, let's learn about our product and what it does.
The Product Management team is responsible for aligning deliverables to parts of the company. Here is how your teammate described how our PM team works: product managers are software developers
The product roadmap is a guide to the short and long term goals for our product. It helps us prioritize our work. Take some time today to review it and get a sense of where we are headed: we don't have one
It's important to know who the customer is for our products. Your teammate described our customers like this: open source community and nonprofits who need chatbots
Task: It's important that you understand the scope of our team's responsibilities. Your teammate said our team is responsible for the following systems and platforms: `qary`, `django-qary`, `nlpia`, and `api.qary.me/bot`. Your goal today is to make sure you understand the responsibilities that fall to our team.

### Process_Containers
OK, let's talk containers.
We create and deploy apps using Docker, Kubernetes.
We use tools to manage containers. The ones we use are: Docker, Kubernetes
Task: Today, read the documentation on our container management tools: no
Next, we'll cover configuration management tools. We use the following tools: GitLab.
Task: Please read this documentation: we don't use containers

### Process_L3
We're going to spend some time in the docs next.
Today you should spend some time learning about the databases we use and are responsible for. This is how your teammate described them: postgres and elastic search
Man, I love documentation! 😍 Here's our documentation on the databases: no
Today, you should find out where templates and stylesheets are kept. You can find the templates and stylesheets that you will use here: not needed
You've probably used some AWS tools in the past. We use the following AWS tools: We prefer to use Digital Ocean, and Heroku. We'll use GCP or AWS only if a customer demands it..
Task: Here's what you need to know about them: nope
Task: Phew! That's a lot of documentation. I hope you set aside some time for reading today. Bookmark the links you think you'll use the most. I'll check in with you later to see how much progress you've made.

### Process_Codebase
Let's dig into the codebase!
Automation tools can speed up your work. We use the following tools: .gitlab-ci
Documentation for how we use automation tools is here: https://gitlab.com/tangibleai/qary/-/blob/master/.gitlab-ci.yml
Get your head in the clouds. Our cloud management practices, that is! Poke around the documentation store for some cloud management resources, or ask someone if you can't find anything.

Monitoring systems and metrics help us understand the state of the applications that we work on. We use the following monitoring systems: Yes:
Take some time to read the documentation here: We track developer impact on coverage (https://codecov.io/gl/tangibleai/qary) and linting metrics (flake8): https://gitlab.com/tangibleai/qary/-/blob/master/setup.cfg#L179
Task: Your next task is to gain a greater understanding of our codebase. You can find information on the size and state of the current codebase here http://gitlab.com/tangibleai/qary and http://gitlab.com/tangibleai/
