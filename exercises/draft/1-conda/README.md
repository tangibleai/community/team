# Anaconda Package Manager & Content

## Table of Contents
1. [What is Conda?](#what-is-conda)
    a. Individual Packages
2. [Install](#how-to-install)
    a. Windows (#windows)
    b. Linux (#linux)
    c. Mac (#mac)
3. [Uses and Application](#uses-and-application)
4. [Advanced Techniques](advanced-techniques)

### What is Conda?

The Anaconda Package Manager usually referred to as Conda is the foundation of any DataScientist tool kit. The power of Conda is in giving a homogenized design environment which is independent to the users operating system. This means you can use Conda and have the same experience or packages no matter if you are using [Linux](@linux), [Windows](@windows), or [Mac](@mac). This allows groups to work together more efficiently.

#### Individual Packages

Conda is comprised of a few different software packages.
A short summary of each is given below.
Depending on your operating system you may need to access your terminal (command line) differently.

* terminal (Linux or Mac)
* CMD.exe Prompt (Win)
* JupyterLab (Linux, Mac, Win)
* JupyterNotebook (any browser)
* Powershell Prompt (Win)
* PyCharm - A notable Python IDE which is commonly used in the industry. For [TangibleAI](@tangibleai) we prefer to use [Sublime Text 3](@sublimetext)
* QtConsole (Win and some Linux)
* Spyder - Another Python IDE which is less resource intensive and simpler then Pycharm. Many people have compared it to RStudio.
* Glueviz
* Orange 3
* RStudio

### Install

Installation of Conda is relatively painless. A link to the download can be found [here](https://www.anaconda.com/products/individual) and is free. Once installed you can access Conda by clicking on a shortcut via your gui or running the appropriate command.

#### Windows
#### Linux
#### Mac

#### Uses and Applications

#### Advanced Techniques
