Independent truth and privacy-focused search engines:

# tags: [URL, wiki, index, advertisement, quality, country, org, privacy]
- [MetaGer](metager.org)
    - [wiki/MetaGer](en.wikipedia.org/wiki/MetaGer)
    - Ad-free, high recall & precision 
    - Germany
    - nonprofit
    - University of Hanover+
    - GDPR
- [You.com](you.com)
    - LLM at you.com/chat
- [qwant.com](en.wikipedia.org/wiki/Qwant)
    - low-Ad (only rank-1 position)
    - high recall & precision, France, for-profit, Pertimm, GDPR]
- [Brave.com](brave.com)
    - 100% independent
    - crypto dark patterns?
- [Duck.com]
    - 10% independent?
    - deal with Mozilla or Google?

 
