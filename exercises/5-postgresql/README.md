# PGAdmin4

Pgadmin4 is a python web application similar to phppgadmin.
First create directories for managing pgadmin libs and logs.

```bash
sudo mkdir /var/lib/pgadmin
sudo mkdir /var/log/pgadmin
sudo chown $USER /var/lib/pgadmin
sudo chown $USER /var/log/pgadmin
```

Next create a Python virtual environment to hold all of pgadmin's dependencies.

```bash
BASE_DIR=$HOME/code/tangibleai/public/pgadmin4
pip install --upgrade virtualenv build wheel twine pip poetry pipdeptree
mkdir -p $BASE_DIR
cd $BASE_DIR
python -m virtualenv .venv
source .venv/bin/activate
```

Now you can install pgadmin from pypi.org with pip.

```bash
pip install pgadmin4
```

You can only launch the pgadmin webapp if you have first activated the virtualenv where it was installed. 

```bash
source $HOME/code/tangibleai/public/pgadmin4/.venv/bin/activate
pgadmin4 &
```

When you first launch pgadmin it will ask you to create an account by entering your e-mail and a password.
You can then use those credentials to log into your pgadmin dashboard where you can create and manage your PostgreSQL databases:

```bash
firefox http://127.0.0.1:5050/browser/
```
