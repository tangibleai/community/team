# Job Search

Half of my jobs I've gotten from inside connections at a company (invitations to interview).
The other half are from recruiters contacting me.


- build your social network including both employers and junior developers, helping others get hired. connecting your contacts to jobs they might like.
- educational (teach DS, ML, or Python programming) general-interest posts on linked-in
- create mastodon and build your followers (so that algorithm doesn't hide you from employers)
- when asked an interview question, always try to ask a clarifying question
- several small posts about different aspects of your project (one post per slide)
- pad your resume with the skills a recruiter needs for a particular job post, while quickly learning those skills before the interview
- customize your resume for each high likelihood job opening