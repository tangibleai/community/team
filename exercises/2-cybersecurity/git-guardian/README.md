# GitGuardian

https://github.com/GitGuardian/ggshield

https://docs.gitguardian.com/internal-repositories-monitoring/integrations/git_hooks/pre_commit

## Global pre-commit hook


install in conda base or outside an environment in order for it to be accessible globally
```bash
pip install ggshield
```

To install pre-commit globally (for all current and future repos):

    1. Create an API key within the [API section](https://dashboard.gitguardian.com/api) of your GitGuardian workspace.
    2. Add this API key to the GITGUARDIAN_API_KEY environment variable of your development environment.
    3. Execute the following command:

```bash
ggshield install --mode global
```

It will:

    - verify that if a global hook folder is defined in the global git configuration.
    - create the ~/.git/hooks folder (if needed).
    - create a pre-commit file which will be executed before every commit.
    - give executable access to this file.
