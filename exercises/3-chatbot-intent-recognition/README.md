# Chatbot Intent Recognition

A rule-based chatbot needs to recognize the users intent based on their utterance (text message) in order to attempt to fulfill that intent.

In machine learning, when your goal is to classify objects as one of many different different categories, that is called a classification problem.
So you first need to label some text messages or documents with the categories or topics you'd like your model to recognize.
Machine learning models are functions that can be automatically programmed by fitting them to a dataset of labeled examples.

You can load some example labeled utterances from the Rori project here:

```ipython
>>>  from pathlib import Path
>>>  import pandas as pd
>>>  with next(Path.cwd().glob('*.csv')).open() as f:
>>>      df = pd.read_csv(f)
>>>  df = df[df.columns[:2]]
>>>  df = df.dropna()
>>>  df.head()
        Utterance     Label
0       skip this      skip
1  this is stupid      skip
2  this is stupid    harder
3  this is stupid  feedback
```

The other side of the classification coin is "regression".
A regression problem is when you are trying to predict a continuous numerical value.
It turns out you can do both simultaneously with a `LogisticRegression` machine learning model.
Since we'd like to have a continuous confidence or probability score for our category predictions, and a Logistic Regression is perfect for this kind of problem.

Load an untrained LogisticRegression algorithm or class.
Then instantiate(construct or initialize) an untrained model with the default hyperparameters:

```python
>>>  from sklearn.linear_model import LogisticRegression
>>>  model = LogisticRegression()
>>>  help(model.fit)
fit(X, y, sample_weight=None)
Fit the model according to the given training data.

    Parameters:
    X: {array - like, sparse matrix} of shape(n_samples, n_features)
        Training vector, where `n_samples` is the number of samples and
        `n_features` is the number of features.

    y: array - like of shape(n_samples,)
        Target vector relative to X.
```

So the documentation says that we need to create a vector of numbers to describe each object(utterance) in our dataset.
This is what an encoder or embedder is good for.
An encoder takes a sequence of natural language words, like the ones in our utterances.
And the encoder outputs a single vector that encapsulates(encodes) the meaning of that text in a single vector.
So you can use a list comprehension to iterate through all of your utterances and encode them into vectors.
Then you can stack them all into a 2 - D numpy array or matrix called `X`:


```python
>>>  import numpy as np
>>>  X = np.array([list(encoder.encode(x)) for x in df['Utterance']])
>>>  y = df['Label']
>>>  X.shape
(138, 384)
```

Machine learning models are functions that can be automatically programmed by fitting them to a dataset of labeled examples.
Sometimes this can be challenging if you have more features than you have examples.
We have more than double the number of dimensions in our feature vectors than we have examples texts.
So we have a "wide" dataset that could make it too easy for the LogisticRegression to fit perfectly to the training set, but perform poorly on the test set and in the real world.
This is like a math student memorizing all the answers to all the quiz questions in a textbook without understanding the underlying patterns and reasons behind those answers.
This is called "overfitting" in machine learning.
But we'll deal with that later.

In machine learning it is called "training" when you give your model both the questions(feature vectors in `X`) and the answers(labels or categories in `y`).
You can "train a model" in scikit - learn by calling the model's "fit()" method:

```python
>>>  model.fit(X, y)
LogisticRegression()
>>>  model.coef_
array([[0.00325773, 0.05756942, 0.06338831, ..., -0.0044367,
         0.00416221, 0.02920555],
       [0.09555886, -0.03279714, -0.10966969, ..., 0.00574765,
         0.02486469, -0.01015629], ...
>>>  model.score(X, y)
0.49...
```

Because there are a lot of ambiguous texts in the dataset you loaded, and these texts are labeled with several different categories, this caused your your model to only achieve 49 % accuracy.
Take a look at some of these predictions:

```python
>>>  y_pred=model.predict(X)
>>>  y_pred
array(['exit', 'harder', 'harder', 'exit', 'exit', 'hint', 'exit', 'exit',
       'exit', 'exit', 'exit', 'exit', 'exit', 'exit', 'exit', 'exit', ...
>>>  df.head()
        Utterance   Label
0       skip this    skip
1  this is stupid    skip
2  this is stupid  harder
3        I'm done    exit
4            quit    exit
```

That's a lot of `exit`s that were mislabeled(perhaps should have been "skip").

Another thing that is limiting our model's accuracy is the imbalance in the labels we took the time to create in the dataset.
There are a lot of "exit" intent labels, but not a lot of "skip" or "too fast" labels.

The `sklearn.LogisticRegression` model has a `class_weight` hyperparameter that you can set to `"balanced"` which will automatically balance out the biases in your dataset by boosting the weights of your minority class labels:

```python
>>>  model.fit(X, y)
>>>  model.score(X, y)
0.659...
>>>  y_pred=model.predict(X)
```

That's a little better.
But you will probably want to create a multi - label classifier or tagger that can handle the ambiguity a little better.
And you'll need to create training and test sets to "cross validate" your model and try to predict how well it will do in the real world, by testing its accuracy on an unseen testset.
Check out the scikit - learn `train_test_split()` function for more details.

The confusion matrix can help you spot particular labels where it might help to gather some more data.
Look for off - diagonal hot(red or white) squares in the confusion matrix:

```python
>>>  df_probas=pd.DataFrame(model.predict_proba(X), columns=model.classes_)
>>>  df_probas['y_pred']=y_pred
>>>  df_probas=df_probas.groupby('y_pred').mean()[:'y_pred']
```

```python
>>>  import matplotlib.pyplot as plt
>>>  import seaborn as sns

>>>  sns.heatmap(df_probas, annot=True)
>>>  df_probas['y_pred']=y_pred
>>>  df_probas=df_probas.groupby('y_pred').mean()[:'y_pred']
>>>  sns.heatmap(df_probas, annot=True)
>>>  plt.tight_layout()
>>>  plt.show()
```

[![heatmap.png](./heatmap.png)](./heatmap.png)
