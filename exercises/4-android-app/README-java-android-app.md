# Build your first Java Android App

## References

- [Install Android Studio on Linux](https://dev.to/janetmutua/installing-android-studio-on-ubuntu-2204-complete-guide-1kh8) by Janet Mutua
- [Install Android Studio on Linux](https://www.makeuseof.com/windows-android-studio-setup/) by Rishabh Chauhan

## Prerequisites

Before you get started, make sure you are using a "Git Bash" terminal in Windows (rather than CMD or PowerShell).
As a developer you need access to standard commands for downloading and manipulating files.
Without these tools you will not be able to use 99% of the servers or instructional videos on the Internet.

* `wget` to download software and data from the Internet
* `cd` and `ls` to change your working directory and list the files in a directory
* `cp`, `mv` and `rm` to copy, move, and remove files and directories.

Obviously you need the `javac` and `java` commands as well, which are installed with the JDK (Java Developer Kit).
OpenJDK version 18 or 19 will both work with Android Studio.
The double-pipe (`||`) is the OR symbol in bash, which will force it to use whichever package manager you have on your particular flavor of Linux.
If you are on Windows, you will have to 

```bash
sudo apt install openjdk-18-jdk \
    || sudo yum install openjdk-18-jdk
```

If you are using 64-bit Linux as your OS, you may need to install some some 32-bit (i386) libraries that Android Studio relies on:

```bash
sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386 \ 
    || sudo yum install zlib.i686 ncurses-libs.i686 bzip2-libs.i686 
```

## Install Android Studio

You can find the latest version of Android Studio on [developer.android.com/studio/archive](https://developer.android.com/studio/archive).
You have to accept the Terms of Service before you can see the list of releases.
For this miniproject you can use the "2023.2.1.14" version at the following direct download links:

- Windows (64-bit): [dl.google.com/dl/android/studio/ide-zips/android-studio-2023.2.1.14-windows.zip](https://dl.google.com/dl/android/studio/ide-zips/android-studio-2023.2.1.14-windows.zip) (1.2 GB)
- Linux: [dl.google.com/dl/android/studio/ide-zips/android-studio-2023.2.1.14-linux.tar.gz](https://dl.google.com/dl/android/studio/ide-zips/android-studio-2023.2.1.14-linux.tar.gz) (2.0 GB)
- Mac (Apple Silicon): [dl.google.com/dl/android/studio/ide-zips/android-studio-2023.2.1.14-mac_arm.zip](https://dl.google.com/dl/android/studio/ide-zips/android-studio-2023.2.1.14-mac_arm.zip) (1.2 GB)
- Mac (Intel): [dl.google.com/dl/android/studio/ide-zips/android-studio-2023.2.1.14-mac.zip](https://dl.google.com/dl/android/studio/ide-zips/android-studio-2023.2.1.14-mac.zip) (1.2 GB)

And then you can use `wget` to download the `android-studio` desktop application as a zipped TAR file.
It's an executable so to install it you just need to put it somewhere in your `$PATH` and make sure it has the permissions you need to run it.

```bash
ARCH=linux  # 'linux' 'windows' 'mac' OR 'mac_arm' 
VER=2023.2.1.14  # more versions here: developer.android.com/studio/archive
FILENAME="android-studio-$VER-$ARCH.tar.gz"
wget https://dl.google.com/dl/android/studio/ide-zips/$VER/$FILENAME
gunzip $FILENAME
sudo mv android-studio /opt/
sudo ln -sf /opt/android-studio/bin/studio.sh /usr/local/bin/android-studio
```

You can now launch Android Studio by running the `android-studio` command from any prompt.
But to give yourself an Icon on your Desktop you can create a Desktop file:

```bash
sudo echo << EOF > sudo tee /usr/share/applications/android-studio.desktop
[Desktop Entry]
Version=1.0
Type=Application
Name=Android Studio
Comment=Android Studio
Exec=bash -i "/opt/android-studio/bin/studio.sh" %f
Icon=/opt/android-studio/bin/studio.png
Categories=Development;IDE;
Terminal=false
StartupNotify=true
StartupWMClass=jetbrains-android-studio
Name[en_US]=android-studio.desktop
EOF
```


