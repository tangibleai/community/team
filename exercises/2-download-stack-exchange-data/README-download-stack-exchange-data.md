# Download your StackExchange data

# References
* StackExchange Question: [How do I export my questions and answers](https://meta.stackexchange.com/q/176293/172724)
* StackExchange[API Docs](http://api.stackexchange.com/)
* StackExchange[Data Explorer](https://data.stackexchange.com/stackoverflow/queries)
* Python API Client: `pip install` [`pystex`](https://pypi.org/project/pystex/)

# StackExchange data explorer
Log into your StackExchange account before running queries in the Data Explorer, so you don't have to deal with captchas.

The[Data Explorer](https://data.stackexchange.com /) lists the most popular SQL queries, such as [this one to find your vote counts](https://data.stackexchange.com/stackoverflow/queries) for each tag:

```sql
-- How many upvotes do I have for each tag?
-- how long before I get tag badges?

DECLARE @ UserId int =  # UserId##

SELECT --TOP 20
    TagName,
    COUNT(*) AS UpVotes
FROM Tags
    INNER JOIN PostTags ON PostTags.TagId = Tags.id
    INNER JOIN Posts ON Posts.ParentId = PostTags.PostId
    INNER JOIN Votes ON Votes.PostId = Posts.Id and VoteTypeId = 2
WHERE
    Posts.OwnerUserId = @UserId
GROUP BY TagName
ORDER BY UpVotes DESC
```

# Download your posts

You can find your `UserID` by visiting your stack exchange profile page.
Mine is at [](https://stackoverflow.com/users/623735/hobs).

Visit the data explorer[query composer](https://data.stackexchange.com/stackoverflow/query/new) and create a query like this to get your data. Don't forget to replace my `UserID` (623735) with yours!

```sql
-- Find your UserID by finding the int in the URL for your StackOverflow profile page.
-- Mine profile page is at https://stackoverflow.com/users/623735/hobs

DECLARE @ UserID int = 623735

SELECT *
FROM Posts
WHERE
    Posts.OwnerUserID = @UserID
```

To also retrieve the Posts where you were the last editor:

```sql
-- Find your UserID by finding the int in the URL for your StackOverflow profile page.
-- Mine profile page is at https://stackoverflow.com/users/623735/hobs

DECLARE @ UserID int = 623735

SELECT *
FROM Posts
WHERE
    Posts.LastEditorUserID = @UserID
```

# StackExchange API

The `pystex` package would not install for me, so I used the `stackapi` package instead:

```bash
pip install stackapi
```

To retrieve the top 500 Python questions(based on upvotes) use the following code:

```python
from stackapi import StackAPI
client = StackAPI('stackoverflow')
questions = client.fetch(
    'questions',
    min=20,
    tagged='python',
    sort='votes',
    filter='withbody'
)
```

This will only retrieve the question titles and IDs.
If you want the question content you will need to retrieve those questions with a separate query.
You probably want to retrieve the answers along with that.
But before you start downloading a bunch of data, you need to obey the throttling limits on StackExchange: 10, 000 requests per day, or one request every 10 seconds.
A cronjob that appends a jsonlines file is probably your best bet.

To find the answers for those 500 questions you need to iterate through the `'items'` key for the questions response.
You may also want to periodically print the `'quota_remaining'` and `'backoff'` flag so you understand why the `stackapi` package may be slowing down(to obey the `'backoff'` value in the API response).

Here's the unthrottled code I used that got me blocked after:

```python
from tqdm import tqdm
answers = []

for q in tqdm(questions['items']):
    qans = client.fetch(f'questions/{q["question_id"]}/answers', filter='withbody')
    answers.append(qans)
    print(qans['quota_remaining'], qans[-1]['backoff'])
```

Here's the error message I got when I kept hitting the API without any delay.
Basically I was told to wait 24 hours.

```python
('https://api.stackexchange.com/2.3/answers/?pagesize=100&page=1&filter=default&question_id=4383571&min=1&sort=votes&site=stackoverflow',
 502, 'throttle_violation', 'too many requests from this IP, more requests available in 84572 seconds')
```

Looks like I had retrieved 29000 answers to 58 of the 500 questions whose titles and ID numbers I had retrieved earlier.

```python
>>> sum(len(a['items']) for a in answers)
29000
```

To join the tables in these lists of json responses, the `'items'` keys contain the actual table data.
So that's probably what you want to load with `pd.normalize_json` or whatever data processing script you have for loading json data into the data format that you need.


### JSONLines

You probably want to incrementally save the responses from StackOverflow in a database or a `jsonlines` file.
Here's how to save all your data at once in the jsonlines format (not really what you want to do, because it's not incremental):


```python
>>> import jsonlines
>>> !mkdir -p data/corpus_stackoverflow
>>> with jsonlines.open('data/corpus_stackoverflow/questions.jsonlines', 'a') as qfile:
>>>     qfile.write_all([questions])
402996
>>> fout.close()
>>> with jsonlines.open('data/corpus_stackoverflow/answers.jsonlines', 'a') as afile:
>>>     afile.write_all(answers)
15112283
```

Notice that the accumulated list of answers contains more than 15MB of data for only 58 questions.
Check out a couple of the answers for the first question:

```python
>>> answers[0]['items'][0]
{'owner': {'account_id': 559988,
  'reputation': 467527,
  'user_id': 922184,
  'user_type': 'registered',
  'profile_image': 'https://i.stack.imgur.com/h7WDB.jpg?s=256&g=1',
  'display_name': 'Mysticial',
  'link': 'https://stackoverflow.com/users/922184/mysticial'},
 'is_accepted': True,
 'score': 34929,
 'last_activity_date': 1706021923,
 'last_edit_date': 1706021923,
 'creation_date': 1340805402,
 'answer_id': 11227902,
 'question_id': 11227809,
 'content_license': 'CC BY-SA 4.0'}
>>> len(answers[0]['items'])
500
>>> answers[0]['items'][-1]
{'owner': {'account_id': 41046,
  'reputation': 38750,
  'user_id': 119212,
  'user_type': 'registered',
  'accept_rate': 78,
  'profile_image': 'https://www.gravatar.com/avatar/b19d877190d625d9f73bb69a0e093081?s=256&d=identicon&r=PG',
  'display_name': 'instanceof me',
  'link': 'https://stackoverflow.com/users/119212/instanceof-me'},
 'is_accepted': True,
 'score': 2767,
 'last_activity_date': 1569958699,
 'last_edit_date': 1569958699,
 'creation_date': 1279041167,
 'answer_id': 3239600,
 'question_id': 3239598,
 'content_license': 'CC BY-SA 4.0'}
```

Notice that the `question_id`s are not the same (11227809 != 3239598 != 231767)!
I fixed the query to use `filter='withbody'` and the `question/{":".join(question_ids)}/answers` endpoint so that the correct answers are retrieved.

And neither IDs match the question_id from the list of questions ():

