# README.md

## NLPiA Video Series

3. May 8: Ch 3 - TFidf Vectors
4. May 15: Ch 4 - Semantic Analysis
5. May 22: Ch 5 - Word brain (neural networks)
6. May 29: Ch 6 - Word embeddings 
7. June 5: Ch 7 - CNNs
8. June 12: Ch 8 - RNNs
9. June 19: Ch 9 -Transformers
10. June 26: Ch 10 - LLMs
