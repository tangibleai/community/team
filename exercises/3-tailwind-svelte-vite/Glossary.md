# Glossary


RSC = react server component (server side renders using Next?), must create 2 redux (state management) stores (frontend and backend) and both must be passed as arguments or imports to all RSC component server calls. 

RCC = React Client Component RCC = single page application where you use NextJS instead of Next. This is the architecture that Robert Campbell used on his Django+React app for a client in Atlanta, launching in March 2024.

SPA = Single Page App

SvelteKit is Svelte's equivalent for `Next` RSC server-side-rendering (not NextJS)

