#!/usr/bin/env bash
echo "Creating functions like *workon()* in $HOME/bin/bash_functions.sh: $0..."

# For more info on exit status codes such as exit 1, exit 2, exit 126, 127, 128 and 255:
# https://www.redhat.com/sysadmin/exit-codes-demystified

function battery_level {
    echo hi
    pyscript=" \
        finf = '/sys/class/power_supply/BAT1/charge_full'; \
        find = '/sys/class/power_supply/BAT1/charge_full_design'; \
        finn = '/sys/class/power_supply/BAT1/charge_now'; \
        pct = float(open(finn).readlines()[0]) / float(list(open(finf))[0]); \
        print(pct); "
    python -c $pyscript
    }


function rmpyc {
    # FIXME add confirmation prompt after running find command without doing anything
    echo "removing all *.py[co] files..."
    find . -type f -name "*.py[co]" -delete
    echo "removing all __pycache__ dirs..."
    find . -type d -name "__pycache__" -print -delete
    echo "removing all *.egg-info dirs"
    find . -type d -name "*.egg-info" -print -exec rm -rf {} \;
    }

function run_customized_workon {
    scripts_dirs=( "." "./scripts" "$HOME" )
    # Look for shell env or activate scripts in .workon .venv venv .env
    # But only source one of them
    for scripts_dir in "${scripts_dirs[@]}"
    do

        workon_config=".workon"
        full_path="$scripts_dir/$workon_config"
        if [[ -f "$full_path" ]]; then
            echo "FOUND WORKON CONFIG: $full_path"
            source "$full_path"
            break
        fi

        VENVDIR=".venv"
        venv_activate="$VENVDIR/bin/activate"
        full_path="$scripts_dir/$venv_activate"
        if [[ -f "$full_path" ]];
        then
            echo "FOUND $VENVDIR: $full_path"
            source "$full_path"
            break
        fi

        VENVDIR="venv"
        venv_activate="$VENVDIR/bin/activate"
        full_path="$scripts_dir/$venv_activate"
        if [[ -f "$full_path" ]];
        then
            echo "FOUND $VENVDIR: $full_path"
            source "$full_path"
            break
        fi

        full_path="$scripts_dir/.env"
        if [[ -f "$full_path" ]];
        then
            echo "FOUND .env: $full_path"
            source "$full_path"
            break
        fi

        full_path="$scripts_dir/secrets.sh"
        if [[ -f "$full_path" ]];
        then
            echo "FOUND secrets.sh: $full_path"
            source "$full_path"
            break
        fi
    done
}

function find_venv_activate {
    echo "Looking for $1/venv/bin/activate script..."
    }


function find_conda_activate {
    echo "Looking for virtual environment named '$1'"
    full_working_path=$2
    echo "full_working_path=$full_working_path"
    parent_dir_path=$(dirname $PWD)
    parent_dir_name=$(basename $parent_dir_path)
    echo "parent_dir_path=$parent_dir_path"
    echo "parent_dir_name=$parent_dir_name"

    conda_sufs=( "" "env" "_env" "37" "38" "36" "27" )
    conda_dirs=( "$HOME/anaconda3" "$HOME/opt/anaconda3" "/usr/opt/anaconda3")
    for conda_suf in "${conda_sufs[@]}" ;  do
        for conda_dir in "${conda_dirs[@]}" ; do
            env_name="$1$conda_suf"
            full_conda_path="$conda_dir/envs/$env_name"
            # echo "checking for conda env in $full_conda_path ..."
            if [[ -d "$full_conda_path" ]]; then
                if [[ -n "$env_name" ]]; then
                    break
                fi
            fi
        done
        if [[ -d "$full_conda_path" ]]; then
            if [[ -n "$env_name" ]]; then
                break
            fi
        fi
    done
    if [[ -d "$full_conda_path" ]]; then
       echo "found $full_conda_path"
    else
        for conda_suf in "${conda_sufs[@]}" ;  do
            for conda_dir in "${conda_dirs[@]}" ; do
                env_name="$parent_dir_name$conda_suf"
                full_conda_path="$conda_dir/envs/$env_name"
                # echo "checking for conda env in $full_conda_path ..."
                if [[ -d "$full_conda_path" ]]; then
                    if [[ -n "$env_name" ]]; then
                        break
                    fi
                fi
            done
            if [[ -d "$full_conda_path" ]]; then
                if [[ -n "$env_name" ]]; then
                    break
                fi
            fi
        done
    fi
    if [[ -d "$full_conda_path" ]]; then
        echo "ACTIVATING CONDA ENV: $env_name ($full_conda_path)"
        conda activate $env_name
    fi
    }

function workon {
    CONDA=''

    if [[ -n "$3" ]]; then
        if [[ "$3" == "conda" ]]; then
            CONDA="$3"
            conda deactivate
            conda activate base
        fi
    fi

    if [[ -z "$1" ]]; then
        echo "Usage: workon DIR_OR_ENV_NAME"
        return 0
    fi

    echo "Time to workon $1, looking for source code dir first ..."
    codedir="$HOME/code" 
    taidir="$codedir/tangibleai"
    base_dirs=()
    while IFS= read -r line; do
        base_dirs+=( "$line" )
    done < <( find $codedir -type d -maxdepth 2 )
    while IFS= read -r line; do
        base_dirs+=( "$line" )
    done < <( find $taidir -type d -maxdepth 2 )
    base_dirs+=( "$taidir/community" )
    sub_dirs=( "moia" "slcc" "" "wip" "teach" "ucsd" "django_project/frontend" "django_project" "frontend" "other" "projects" "archive" )
    for sub_dir in "${sub_dirs[@]}" ; do
        for base_dir in "${base_dirs[@]}" ; do
            if [[ -n "$1" ]]; then
                if [[ -n "$subdir" ]]; then
                    full_path="$base_dir/$sub_dir/$1"
                else
                    full_path="$base_dir/$1"
                fi
            else
                full_path="$base_dir/$sub_dir"
            fi
            # echo "Checking for .git/ path: ${full_path}/.git/"
            if [[ -d "$full_path" ]]; then
                if [[ -d "$full_path/.git" ]]; then
                    echo "FOUND DIR: $full_path"
                    # cd "$full_path"
                    break ;
                else
                    printf ""
                    # echo "not found: $full_path"
                fi
            fi
        done
        if [[ -d "$full_path/.git" ]]; then
            echo "FOUND .git: $full_path/.git"
            break ;
        fi
    done

    # echo "[[ -d '${full_path}' ]]"
    if [[ -d "$full_path" ]] ; then
        echo "CHANGING WORKING DIR: $full_path"
        cd "$full_path"
        if [[ -n "$CONDA" ]]; then
            find_conda_activate $1 $full_path
        fi
        run_customized_workon
        return 0
    else
        taidir="$HOME/code/tangibleai"
        base_dirs=( "$taidir/moia" "$taidir/slcc" "$taidir/prosocialai" "$taidir/proai" "$taidir" "$taidir/../teach" "$taidir/../chatbot" "$taidir/.." "$taidir/../src" )
        sub_dirs=( "moia" "slcc" "" "wip" "teach" "ucsd" "django_project/frontend" "django_project" "frontend" "other" "projects" "archive" )
        for sub_dir in "${sub_dirs[@]}" ; do
            for base_dir in "${base_dirs[@]}" ; do
                if [[ -n "$1" ]]; then
                    if [[ -n "$subdir" ]]; then
                        full_path="$base_dir/$sub_dir/$1"
                    else
                        full_path="$base_dir/$1"
                    fi
                else
                    full_path="$base_dir/$sub_dir"
                fi
                if [[ -d "$full_path" ]]; then
                    break
                fi
            done
            if [[ -d "$full_path" ]]; then
                echo "FOUND DIR: $full_path"
                break
            fi
        done
    fi
    if [[ -d "$base_dir" ]] ; then
        echo "CHANGING WORKING DIR: $full_path"
        cd "$full_path"
        run_customized_workon
        return 0
    else
        echo "Couldn't find a directory by that name: $1"
    fi
}

function is_running {

    if [[ -z "$1" ]]; then
        echo "Usage: running PATTERN_FOR_PROCESS_PS"
    else
        if pidof -x "$1" -o $$ >/dev/null; then
            # This is the return value that gets gobbled by the variable it's assigned to
            echo "$1 is already running"
            return 0
        else
            echo ""
            return 0
        fi
    fi
}
